# FAQ - StratoTerminal 2 

StratoTermianl to program desktopowy służacy do odbierania i przetwarzania telemetrii z golndoli stratosferycznej.

Funkcje programu:

- odbieranie i wyświetlanie danych telemetrii wysyłanych przez port COM.
- zapis telemetrii do 3 formatów logów (raw, csv, program log).
    - raw - zapisana surowa wersja każdej ramki z telemetrii w postaci odebranej z urządzenia nadającego.
    - csv - przetworzona wersja logów zapisana w postaci csv, w tej wersji znajduje się zapis ramek sprawdzonych pod kątem poprawności i wcześniej przekonwertowanych na odpowienie typy danych.
    - program log - logi programu, połączone z ramkami w postaci surowej.
- pokazywanie ścieżki lotu gondoli stratosferycznej na żywo na mapie 2D.
- wysyłanie poleceń do urządzenia.
- wysyłanie danych do bazy danych.


![Strato Terminal 2 - Główny widok](http://fol.cba.pl/Images/StratoTermianl2Screen.PNG)


# Zasada działania:
Przez port COM przesyłana jest ramka w specjalnym formacie. Gdy ramka jest odebrana to pojawia się nowy log w sekcji Terminal, pozycja na mapie oraz najnowsze dane z ramki w sekcji Telemetry.

## Postać Ramki

Przykładowe ramki (log raw):
```
#A308;234313;24;22;0.000;3;08;38;53.54013;14.54044;0.610;0;000000;;
#A309;234315;25;22;0.000;3;08;38;53.54012;14.54044;1.399;0;000000;;
#A310;234317;25;23;0.000;3;07;38;53.54012;14.54044;1.940;0;000000;;
#A311;234319;25;22;0.000;3;08;38;53.54013;14.54045;0.813;0;000000;;
#A312;234327;24;22;0.000;3;06;40;53.54010;14.54046;6.988;40;000000;;
#A325;234403;24;22;0.000;1;00;??;??;??;??;??;000000;;
```

Skład ramki, podany w kolejności od lewej do prawej, separatorem ramki jest znak średnika ';':

- Id ramki, składa się z nazwy urządzenia i numeru ramki
- czas utworzenia ramki UTC
- temperatura wewnętrzna w gondli
- temperatura zewnętrzna
- bateria, napięcie
- fix, informacja, o tym jakie dane o pozycji znajdują się w ramce. Dozwolone trzy wartości:3 – pełna pozycja zwrócona przez GPS, czyli szerokość geograficzna, długość geograficzna i wysokość. 2 – pozycja zwrócona bez wysokości urządzenia. 1  brak pozycji odczytanej przez GPS.
- Satelity – jakość sygnału, ilość satelitów które zlokalizowało GPS w urzadzeniu.
- wysokość w metrach
- dlugość geograficzna
- szerokość geograficzna
- prędkość horyzontalna
- wznoszenie (m/s)
- iocontrol, input/output control - dane z gondoli na temat podłączonych wejśc wyjść cyfrowych, Ten ciąg znaków może przyjąć dowolną długość w zależności od ilości wejść cyfrowych i składa się z ‘0’ i ‘1’.

# Autor
Fabian Oleksiuk
Jeśli chcesz dowiedzieć więcej na temat projektu napisz [o mnie](mailto:fabbbianoleksiuk@gmail.com).