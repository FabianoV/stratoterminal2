﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StratoTerminal2.Services.Contract.Api;
using StratoTerminal2.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StratoTerminal2.Tests.IoC_Tests.Services
{
    [TestClass]
    public class IBingMapsApiTests
    {
        [TestMethod]
        public void IoC_IsIBingMapApiExists_Successful()
        {
            //Init
            ViewModelLocator vml = new ViewModelLocator();

            //Test
            IBingMapApi apiService = vml.BingMapApi;

            //Assert
            Assert.IsNotNull(apiService);
        }
    }
}
