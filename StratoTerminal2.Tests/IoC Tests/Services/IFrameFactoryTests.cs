﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StratoTerminal2.Services.Contract;
using StratoTerminal2.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StratoTerminal2.Tests.IoC_Tests
{
    [TestClass]
    public class IFrameFactoryTests
    {
        [TestMethod]
        public void IoC_IsIFrameFactoryServiceExists_Successful()
        {
            //Init
            ViewModelLocator vml = new ViewModelLocator();

            //Test
            IFrameFactory frameFactory = vml.FrameFactoryService;

            //Assert
            Assert.IsNotNull(frameFactory);
        }
    }
}
