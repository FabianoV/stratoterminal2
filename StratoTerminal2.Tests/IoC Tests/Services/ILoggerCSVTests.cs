﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StratoTerminal2.Services.Contract;
using StratoTerminal2.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StratoTerminal2.Tests.IoC_Tests
{
    [TestClass]
    public class ILoggerCSVTests
    {
        [TestMethod]
        public void IoC_IsILoggerCSVServiceExists_Successful()
        {
            //Init
            ViewModelLocator vml = new ViewModelLocator();

            //Test
            ILoggerCSV loggerCSV = vml.LoggerCSVService;

            //Assert
            Assert.IsNotNull(loggerCSV);
        }
    }
}
