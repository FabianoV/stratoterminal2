﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StratoTerminal2.Services.Contract;
using StratoTerminal2.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StratoTerminal2.Tests.IoC_Tests.Services
{
    [TestClass]
    public class ILoggerRawTests
    {
        [TestMethod]
        public void IoC_IsILoggerRawServiceExists_Successful()
        {
            //Init
            ViewModelLocator vml = new ViewModelLocator();

            //Test
            ILoggerRaw loggerRaw = vml.LoggerRaw;

            //Assert
            Assert.IsNotNull(loggerRaw);
        }
    }
}
