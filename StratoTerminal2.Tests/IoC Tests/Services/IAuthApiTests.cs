﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StratoTerminal2.Services.Contract.Api;
using StratoTerminal2.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StratoTerminal2.Tests.IoC_Tests.Services
{
    [TestClass]
    public class IAuthApiTests
    {
        [TestMethod]
        public void IoC_IsIFlightsApiExists_Successful()
        {
            //Init
            ViewModelLocator vml = new ViewModelLocator();

            //Test
            IAuthApi apiService = vml.AuthApi;

            //Assert
            Assert.IsNotNull(apiService);
        }
    }
}
