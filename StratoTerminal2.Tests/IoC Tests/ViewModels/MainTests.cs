﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StratoTerminal2.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StratoTerminal2.Tests.IoC_Tests.ViewModels
{
    [TestClass]
    public class MainTests
    {
        [TestMethod]
        public void IoC_IsMainViewModelExists_Successful()
        {
            //Init
            ViewModelLocator vml = new ViewModelLocator();

            //Test
            Main inputOutput = vml.Main;

            //Assert
            Assert.IsNotNull(inputOutput);
        }
    }
}
