﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StratoTerminal2.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StratoTerminal2.Tests.IoC_Tests.ViewModels
{
    [TestClass]
    public class SettingsTests
    {
        [TestMethod]
        public void IoC_IsSettingsViewModelExists_Successful()
        {
            //Init
            ViewModelLocator vml = new ViewModelLocator();

            //Test
            SettingsPanel settings = vml.SettingsPanel;

            //Assert
            Assert.IsNotNull(settings);
        }
    }
}
