﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StratoTerminal2.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StratoTerminal2.Tests.IoC_Tests.ViewModels
{
    [TestClass]
    public class TelemetryTests
    {
        [TestMethod]
        public void IoC_IsTelemetryViewModelExists_Successful()
        {
            //Init
            ViewModelLocator vml = new ViewModelLocator();

            //Test
            Telemetry telemetry = vml.Telemetry;

            //Assert
            Assert.IsNotNull(telemetry);
        }
    }
}
