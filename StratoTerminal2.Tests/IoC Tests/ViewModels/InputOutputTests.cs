﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StratoTerminal2.Services.Contract;
using StratoTerminal2.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StratoTerminal2.Tests.IoC_Tests.ViewModels
{
    [TestClass]
    public class InputOutputTests
    {
        [TestMethod]
        public void IoC_IsInputOutputViewModelExists_Successful()
        {
            //Init
            ViewModelLocator vml = new ViewModelLocator();

            //Test
            InputOutputPanel inputOutput = vml.InputOutput;

            //Assert
            Assert.IsNotNull(inputOutput);
        }
    }
}
