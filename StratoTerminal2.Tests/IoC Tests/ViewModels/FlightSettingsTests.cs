﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StratoTerminal2.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StratoTerminal2.Tests.IoC_Tests.ViewModels
{
    [TestClass]
    public class FlightSettingsTests
    {
        [TestMethod]
        public void IoC_IsFlightSettingsViewModelExists_Successful()
        {
            //Init
            ViewModelLocator vml = new ViewModelLocator();

            //Test
            FlightSettings flightSettings = vml.FlightSettings;

            //Assert
            Assert.IsNotNull(flightSettings);
        }
    }
}
