﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StratoTerminal2.Models;
using StratoTerminal2.Services.Contract;
using StratoTerminal2.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StratoTerminal2.Tests.Frame_Tests
{
    [TestClass]
    public class FrameFactoryTests
    {
        [TestMethod]
        public void Frame_IfNoGPSPositionTimeAndVoltage_Successful()
        {
            //#A3;;-81;-82;0.000;;??;??;??;??;??;??;000000;; - brak pozycji i czasu gps, raczej brak gps, pomiar napięcia odłączony

            //Init
            ViewModelLocator vml = new ViewModelLocator();
            string rawFrame = "#A3;;-81;-82;0.000;;??;??;??;??;??;??;000000;;" + Environment.NewLine;

            //Test
            IFrameFactory factory = vml.FrameFactoryService;
            Frame frame = factory.Create(rawFrame);

            //Assert
            Assert.IsTrue(frame.State == Models.Enums.FrameState.Data);
        }

        [TestMethod]
        public void Frame_IfGPSConnectedAndNoVoltage_Successful()
        {
            //#A1;;-81;-82;0.000;1;00;??;??;??;??;??;000000;; - jak wyżej, ale podłączony gps

            //Init
            ViewModelLocator vml = new ViewModelLocator();
            string rawFrame = "#A1;;-81;-82;0.000;1;00;??;??;??;??;??;000000;;" + Environment.NewLine;

            //Test
            IFrameFactory factory = vml.FrameFactoryService;
            Frame frame = factory.Create(rawFrame);

            //Assert
            Assert.IsTrue(frame.State == Models.Enums.FrameState.Data);
        }

        [TestMethod]
        public void Frame_IfNoGPSPositionAndTimeAndFrameOnCommand_Successful()
        {
            //#A4;;-81;-82;0.000;;??;??;??;??;??;??;000000;|LAST_OK|; - brak brak pozycji i czasu gps, ramka na rozkaz [ostatnia poprawna]

            //Init
            ViewModelLocator vml = new ViewModelLocator();
            string rawFrame = "#A4;;-81;-82;0.000;;??;??;??;??;??;??;000000;|LAST_OK|;" + Environment.NewLine;

            //Test
            IFrameFactory factory = vml.FrameFactoryService;
            Frame frame = factory.Create(rawFrame);

            //Assert
            Assert.IsTrue(frame.State == Models.Enums.FrameState.DataOnDemand);
        }

        [TestMethod]
        public void Frame_IfGPSConnectedAndFrameOnCommand_Successful()
        {
            //#A5;;-81;-82;0.000;;00;??;??;??;??;??;000000;|LAST_OK|;

            //Init
            ViewModelLocator vml = new ViewModelLocator();
            string rawFrame = "#A4;;-81;-82;0.000;;??;??;??;??;??;??;000000;|LAST_OK|;" + Environment.NewLine;

            //Test
            IFrameFactory factory = vml.FrameFactoryService;
            Frame frame = factory.Create(rawFrame);

            //Assert
            Assert.IsTrue(frame.State == Models.Enums.FrameState.DataOnDemand);
        }

        [TestMethod]
        public void Frame_IfGPSConnectedAndFrameOnCommandWithData_Successful()
        {
            //#A7;232608;-81;-82;7.374;;00;??;??;??;??;??;000000;>DATA:52:53:54:54:60:50:59:58|LAST_OK|; 

            //Init
            ViewModelLocator vml = new ViewModelLocator();
            string rawFrame = "#A7;232608;-81;-82;7.374;;00;??;??;??;??;??;000000;>DATA:52:53:54:54:60:50:59:58|LAST_OK|;" + Environment.NewLine;

            //Test
            IFrameFactory factory = vml.FrameFactoryService;
            Frame frame = factory.Create(rawFrame);

            //Assert
            Assert.IsTrue(frame.State == Models.Enums.FrameState.DataOnDemand);
        }

        [TestMethod]
        public void Frame_IfGPSConnectedAndFrameOnCommandWithDataWithFix_Successful()
        {
            //#A20;232627;-81;-82;7.372;1;00;??;??;??;??;??;000000;>DATA:47:48:49:50:51:50:49:48;

            //Init
            ViewModelLocator vml = new ViewModelLocator();
            string rawFrame = "#A20;232627;-81;-82;7.372;1;00;??;??;??;??;??;000000;>DATA:47:48:49:50:51:50:49:48;" + Environment.NewLine;

            //Test
            IFrameFactory factory = vml.FrameFactoryService;
            Frame frame = factory.Create(rawFrame);

            //Assert
            Assert.IsTrue(frame.State == Models.Enums.FrameState.Data);
        }

        [TestMethod]
        public void Frame_IfHasTimeAndNoPosition_Successful()
        {
            //#A149;180445;-81;-82;7.408;1;00;??;??;??;??;??;000000;; - brak pozycji, jest czas

            //Init
            ViewModelLocator vml = new ViewModelLocator();
            string rawFrame = "#A149;180445;-81;-82;7.408;1;00;??;??;??;??;??;000000;;" + Environment.NewLine;

            //Test
            IFrameFactory factory = vml.FrameFactoryService;
            Frame frame = factory.Create(rawFrame);

            //Assert
            Assert.IsTrue(frame.State == Models.Enums.FrameState.Data);
        }

        [TestMethod]
        public void Frame_IfHasTimeAndNoPositionAndOnDemand_Successful()
        {
            //#A150;180447;-81;-82;7.408;;00;??;??;??;??;??;000000;|LAST_OK|; - brak pozycji, jest czas, ramka na rozkaz

            //Init
            ViewModelLocator vml = new ViewModelLocator();
            string rawFrame = "#A150;180447;-81;-82;7.408;;00;??;??;??;??;??;000000;|LAST_OK|;" + Environment.NewLine;

            //Test
            IFrameFactory factory = vml.FrameFactoryService;
            Frame frame = factory.Create(rawFrame);

            //Assert
            Assert.IsTrue(frame.State == Models.Enums.FrameState.DataOnDemand);
        }

        [TestMethod]
        public void Frame_PatternFrame_Successful()
        {
            //#A1059;204350;23;22;7.406;3;05;35;53.54027;14.54059;1.462;5;000000;; - POPRAWNA

            //Init
            ViewModelLocator vml = new ViewModelLocator();
            string rawFrame = "#A1059;204350;23;22;7.406;3;05;35;53.54027;14.54059;1.462;5;000000;;" + Environment.NewLine;

            //Test
            IFrameFactory factory = vml.FrameFactoryService;
            Frame frame = factory.Create(rawFrame);

            //Assert
            Assert.IsTrue(frame.State == Models.Enums.FrameState.Data);
        }

        [TestMethod]
        public void Frame_PatternFrameWithData_Successful()
        {
            //#A23;232653;-81;-82;7.372;3;05;85;53.54005;14.53966;1.118;85;000000;>DATA:41:47:47:48:50:50:48:47; - POPRAWNA + DANE

            //Init
            ViewModelLocator vml = new ViewModelLocator();
            string rawFrame = "#A23;232653;-81;-82;7.372;3;05;85;53.54005;14.53966;1.118;85;000000;>DATA:41:47:47:48:50:50:48:47;" + Environment.NewLine;

            //Test
            IFrameFactory factory = vml.FrameFactoryService;
            Frame frame = factory.Create(rawFrame);

            //Assert
            Assert.IsTrue(frame.State == Models.Enums.FrameState.Data);
        }

        [TestMethod]
        public void Frame_PatternFrameWithDataOnDemand_Successful()
        {
            //#A75;232842;-81;-82;7.330;3;05;70;53.54015;14.53998;2.524;;000000;>DATA:52:53:60:60:62:62:61:59|LAST_OK|; - POPRAWNA + DANE [NA ROZKAZ]

            //Init
            ViewModelLocator vml = new ViewModelLocator();
            string rawFrame = "#A75;232842;-81;-82;7.330;3;05;70;53.54015;14.53998;2.524;;000000;>DATA:52:53:60:60:62:62:61:59|LAST_OK|;" + Environment.NewLine;

            //Test
            IFrameFactory factory = vml.FrameFactoryService;
            Frame frame = factory.Create(rawFrame);

            //Assert
            Assert.IsTrue(frame.State == Models.Enums.FrameState.DataOnDemand);
        }

        [TestMethod]
        public void Frame_DamagedFrameFromAzureSM_Successful()
        {
            //#B1040;221605;20;19;4.661;3;09;??;0.83323;18.30490;0.209#B1041;221606;20;19;4.655;3;09;44;0.83323;18.30490;0.078;44;0111;82.13;

            //Init
            ViewModelLocator vml = new ViewModelLocator();
            string rawFrame = "#B1040;221605;20;19;4.661;3;09;??;0.83323;18.30490;0.209#B1041;221606;20;19;4.655;3;09;44;0.83323;18.30490;0.078;44;0111;82.13;" + Environment.NewLine;

            //Test
            IFrameFactory factory = vml.FrameFactoryService;
            Frame frame = factory.Create(rawFrame);

            //Assert
            Assert.IsTrue(frame.State == Models.Enums.FrameState.Damaged);
        }
    }
}
