﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StratoTerminal2.Models;
using StratoTerminal2.Services.Contract;
using StratoTerminal2.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StratoTerminal2.Tests
{
    [TestClass]
    public class LoggerRawTests
    {
        [TestMethod]
        public void LoggerRaw_IsLoggerHasDefaultConfiguration_Successful()
        {
            //Init
            ViewModelLocator vml = new ViewModelLocator();
            ILoggerRaw logger = vml.LoggerRaw;

            //Test
            logger.Configure();

            //Assert
            Assert.IsTrue(logger.IsConfigured);
        }

        [TestMethod]
        public void LoggerRaw_IsLoggerMayHaveChangeLogPath_Successful()
        {
            //Init
            ViewModelLocator vml = new ViewModelLocator();
            ILoggerRaw logger = vml.LoggerRaw;
            string path = "test_log_raw.log";

            //Test
            logger.Configure(path);

            //Assert
            StringAssert.Contains(logger.Path, path);
        }

        [TestMethod]
        public void LoggerRaw_IsLoggerWritesFrameToFileCorectly_Successful()
        {
            //Init
            ViewModelLocator vml = new ViewModelLocator();
            ILoggerRaw logger = vml.LoggerRaw;
            IFrameFactory factory = vml.FrameFactoryService;
            string path = "test_log_" + Guid.NewGuid() + ".csv";
            string frameRaw = "#A150;180447;-81;-82;7.408;;00;??;??;??;??;??;000000;|LAST_OK|;" + Environment.NewLine;

            //Test
            Frame frame = factory.Create(frameRaw);
            logger.Configure(path);
            logger.LogFrame(frame);

            //Assert
            string[] a = File.ReadAllLines(logger.Path);
            StringAssert.Contains(File.ReadAllLines(logger.Path)[0], "#A150");
        }
    }
}
