﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using StratoTerminal2.ViewModel;
using StratoTerminal2.Services.Contract;
using System.IO;
using StratoTerminal2.Models;

namespace StratoTerminal2.Tests
{
    [TestClass]
    public class LoggerCSVTests
    {
        [TestMethod]
        public void LoggerCSV_IsLoggerHasDefaultConfiguration_Successful()
        {
            //Init
            ViewModelLocator vml = new ViewModelLocator();
            ILoggerCSV logger = vml.LoggerCSVService;

            //Test
            logger.Configure();

            //Assert
            Assert.IsTrue(logger.IsConfigured);
        }

        [TestMethod]
        public void LoggerCSV_IsLoggerMayHaveChangeLogPath_Successful()
        {
            //Init
            ViewModelLocator vml = new ViewModelLocator();
            ILoggerCSV logger = vml.LoggerCSVService;
            string path = "test_log.csv";

            //Test
            logger.Configure(path);

            //Assert
            StringAssert.Contains(logger.Path, path);
        }

        [TestMethod]
        public void LoggerCSV_IsLoggerHasCreatedFileWhenConfigured_Successful()
        {
            //Init
            ViewModelLocator vml = new ViewModelLocator();
            ILoggerCSV logger = vml.LoggerCSVService;
            string path = "test_log_" + Guid.NewGuid() + ".csv";

            //Test
            logger.Configure(path);

            //Assert
            Assert.IsTrue(File.Exists(logger.Path));
        }

        [TestMethod]
        public void LoggerCSV_IsLoggerWritesMessageToFile_Successful()
        {
            //Init
            ViewModelLocator vml = new ViewModelLocator();
            ILoggerCSV logger = vml.LoggerCSVService;
            string path = "test_log_" + Guid.NewGuid() + ".csv";
            string message = "Test message 1234";

            //Test
            logger.Configure(path);
            logger.LogLine(message);

            //Assert
            StringAssert.Contains(File.ReadAllText(logger.Path), message);
        }

        [TestMethod]
        public void LoggerCSV_ISLoggerWritesFrameToFileCorectly_Successful()
        {
            //Init
            ViewModelLocator vml = new ViewModelLocator();
            ILoggerCSV logger = vml.LoggerCSVService;
            IFrameFactory factory = vml.FrameFactoryService;
            string path = "test_log_" + Guid.NewGuid() + ".csv";
            string frameRaw = "#A150;180447;-81;-82;7.408;;00;??;??;??;??;??;000000;|LAST_OK|;" + Environment.NewLine;

            //Test
            Frame frame = factory.Create(frameRaw);
            logger.Configure(path);
            logger.LogFrame(frame);

            //Assert
            string[] a = File.ReadAllLines(logger.Path);
            StringAssert.Contains(File.ReadAllLines(logger.Path)[1], "#A150");
        } 
    }
}
