﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StratoTerminal2.Models.CommandsObjects;
using StratoTerminal2.Services.Contract;
using StratoTerminal2.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StratoTerminal2.Tests.Services_Logic_Tests.Command_Management_Service_Tests
{
    [TestClass]
    public class CommandManagementServiceTests
    {
        [TestMethod]
        public void CommandMgm_IfSettingFileCreatedSuccessfully_Successful()
        {
            //Init
            ViewModelLocator vml = new ViewModelLocator();
            ICommandManagementService commandManagemendService = vml.CommandManagementService;
            List<GroupData> testGroups = new List<GroupData>(){
                new GroupData()
                {
                    GroupName = "First",
                    Commands = new List<CommandData>()
                    {
                        new CommandData() { Name = "JOKE 1", Content = ">JOKE1", Description = "opis długi polecenia" },
                        new CommandData() { Name = "JOKE 2", Content = ">JOKE2", Description = "opis długi polecenia 2" }
                    }
                }
            };
            string settingFilename = "SettingFilenameTest.json";

            //Test
            commandManagemendService.SettingsFilename = settingFilename;
            commandManagemendService.Groups = testGroups;
            commandManagemendService.Save();

            //Assert
            Assert.IsTrue(File.Exists(settingFilename));
        }

        [TestMethod]
        public void CommandMgm_IfSettingFileCreatedSuccessfullyHasContentAfterCreate_Successful()
        {
            //Init
            ViewModelLocator vml = new ViewModelLocator();
            ICommandManagementService commandManagemendService = vml.CommandManagementService;
            List<GroupData> testGroups = new List<GroupData>(){
                new GroupData()
                {
                    GroupName = "First",
                    Commands = new List<CommandData>()
                    {
                        new CommandData() { Name = "JOKE 1", Content = ">JOKE1", Description = "opis długi polecenia" },
                        new CommandData() { Name = "JOKE 2", Content = ">JOKE2", Description = "opis długi polecenia 2" }
                    }
                }
            };
            string settingFilename = "SettingFilenameTest2.json";

            //Test
            commandManagemendService.SettingsFilename = settingFilename;
            commandManagemendService.Groups = testGroups;
            commandManagemendService.Save();

            //Assert
            Assert.IsTrue(File.ReadAllText(settingFilename).Length > 30); //In file must be more than 30 characters with this data
        }

        [TestMethod]
        public void CommandMgm_IfSettingFileOpenedSuccessfully_Successful()
        {
            //Init
            ViewModelLocator vml = new ViewModelLocator();
            ICommandManagementService commandManagemendService = vml.CommandManagementService;
            string validJson = "[" +
            "   {" +
            "      \"GroupName\":\"Reset\"," +
            "      \"Commands\":[" + 
            "         { \"Name\":\"RSTW\", \"Content\":\">RSTW\", \"Description\":\"watchdog reset\" }," +
            "         { \"Name\":\"RST\", \"Content\":\">RST\", \"Description\":\"reset bootloader jump\" }" +
            "      ]" +
            "   }]";
            string settingFilename = "SettingFilenameTest3.json";
            File.WriteAllText(settingFilename, validJson);

            //Test
            commandManagemendService.SettingsFilename = settingFilename;
            commandManagemendService.Open();

            //Assert
            Assert.IsNotNull(commandManagemendService.Groups);
            Assert.IsNotNull(commandManagemendService.Groups[0].Commands);
            Assert.IsTrue(commandManagemendService.Groups[0].GroupName == "Reset");
        }
    }
}
