﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StratoTerminal2.Models.RequestsResults;
using StratoTerminal2.Services.Contract;
using StratoTerminal2.Services.Contract.Api;
using StratoTerminal2.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StratoTerminal2.Tests.Api_Tests.Frame
{
    [TestClass]
    public class CreateFrameTests
    {
        [TestMethod]
        public async Task ApiFrame_CreateFrameResultIsNotNull_Successful()
        {
            //Init
            ViewModelLocator vml = new ViewModelLocator();
            IFramesApi api = vml.FramesApi;
            IFrameFactory frameFactory = vml.FrameFactoryService;
            api.IsTests = true;
            Models.Frame frame = frameFactory.Create("#A12;232842;-81;-82;7.330;3;05;70;53.54015;14.53998;2.524;;000000;;" + Environment.NewLine);
            frame.FlightId = 0;

            //Test
            CreateFrameRequestResult result = await api.CreateFrame(frame);

            //Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task ApiFrame_CreateFrameResultIsHasFlightData_Successful()
        {
            //Init
            ViewModelLocator vml = new ViewModelLocator();
            IFramesApi api = vml.FramesApi;
            IFrameFactory frameFactory = vml.FrameFactoryService;
            api.IsTests = true;
            Models.Frame frame = frameFactory.Create("#A13;232842;-81;-82;7.330;3;05;70;53.54015;14.53998;2.524;;000000;;" + Environment.NewLine);
            frame.FlightId = 3;

            //Test
            CreateFrameRequestResult result = await api.CreateFrame(frame);

            //Assert
            Assert.IsTrue(result.IsSuccessfulyCreated);
        }

        [TestMethod]
        public async Task ApiFrame_CreateFrameResultNoHasError_Successful()
        {
            //Init
            ViewModelLocator vml = new ViewModelLocator();
            IFramesApi api = vml.FramesApi;
            IFrameFactory frameFactory = vml.FrameFactoryService;
            api.IsTests = true;
            Models.Frame frame = frameFactory.Create("#A14;232842;-81;-82;7.330;3;05;70;53.54015;14.53998;2.524;;000000;;" + Environment.NewLine);
            frame.FlightId = 0;

            //Test
            CreateFrameRequestResult result = await api.CreateFrame(frame);

            //Assert
            Assert.IsFalse(result.HasError);
        }
    }
}
