﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StratoTerminal2.Models.RequestsResults;
using StratoTerminal2.Services.Contract;
using StratoTerminal2.Services.Contract.Api;
using StratoTerminal2.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StratoTerminal2.Tests.Api_Tests.Frame
{
    [TestClass]
    public class GetFramesTests
    {
        [TestMethod]
        public async Task ApiFrame_GetFramesResultIsNotNull_Successful()
        {
            //Init
            ViewModelLocator vml = new ViewModelLocator();
            IFramesApi api = vml.FramesApi;
            api.IsTests = true;
            int flightId = 0;

            //Test
            GetFramesFromFlightRequestResult result = await api.GetFrames(flightId);

            //Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task ApiFrame_GetFramesCollectionIsNotNull_Successful()
        {
            //Init
            ViewModelLocator vml = new ViewModelLocator();
            IFramesApi api = vml.FramesApi;
            api.IsTests = true;
            int flightId = 0;

            //Test
            GetFramesFromFlightRequestResult result = await api.GetFrames(flightId);

            //Assert
            Assert.IsNotNull(result.Frames);
        }

        [TestMethod]
        public async Task ApiFrame_GetFramesCollectionIsNotEmpty_Successful()
        {
            //Init
            ViewModelLocator vml = new ViewModelLocator();
            IFramesApi api = vml.FramesApi;
            api.IsTests = true;
            IFrameFactory frameFactory = vml.FrameFactoryService;
            Models.Frame frame = frameFactory.Create("#A7;232842;-81;-82;7.330;3;05;70;53.54015;14.53998;2.524;;000000;;" + Environment.NewLine);
            frame.FlightId = 0;

            //Test
            CreateFrameRequestResult resultCreate = await api.CreateFrame(frame);
            GetFramesFromFlightRequestResult result = await api.GetFrames(frame.FlightId);

            //Assert
            Assert.IsTrue(result.Frames.Count > 0);
        }
    }
}
