﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StratoTerminal2.Models.RequestsResults;
using StratoTerminal2.Services.Contract;
using StratoTerminal2.Services.Contract.Api;
using StratoTerminal2.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StratoTerminal2.Tests.Api_Tests.Frame
{
    [TestClass]
    public class UpdateFrameTests
    {
        [TestMethod]
        public async Task ApiFrame_UpdateFrameResultIsNotNull_Successful()
        {
            //Init
            ViewModelLocator vml = new ViewModelLocator();
            IFramesApi api = vml.FramesApi;
            api.IsTests = true;
            IFrameFactory frameFactory = vml.FrameFactoryService;
            Models.Frame frame = frameFactory.Create("#A1;232842;-81;-82;7.330;3;05;70;53.54015;14.53998;2.524;;000000;;" + Environment.NewLine);
            frame.FlightId = 0;

            //Test
            CreateFrameRequestResult createResult = await api.CreateFrame(frame);
            GetFrameRequestResult get1Result = await api.GetFrame(frame.Id, frame.FlightId);
            frame.SatelitesCount = 14;
            UpdateFrameRequestResult updateResult = await api.UpdateFrame(frame);
            GetFrameRequestResult get2Result = await api.GetFrame(frame.Id, frame.FlightId);

            //Assert
            Assert.IsNotNull(updateResult);
        }

        [TestMethod]
        public async Task ApiFrame_UpdateFrameResultHasNoError_Successful()
        {
            //Init
            ViewModelLocator vml = new ViewModelLocator();
            IFramesApi api = vml.FramesApi;
            api.IsTests = true;
            IFrameFactory frameFactory = vml.FrameFactoryService;
            Models.Frame frame = frameFactory.Create("#A2;232842;-81;-82;7.330;3;05;70;53.54015;14.53998;2.524;;000000;;" + Environment.NewLine);
            frame.FlightId = 0;

            //Test
            CreateFrameRequestResult createResult = await api.CreateFrame(frame);
            GetFrameRequestResult get1Result = await api.GetFrame(frame.Id, frame.FlightId);
            frame.SatelitesCount = 14;
            UpdateFrameRequestResult updateResult = await api.UpdateFrame(frame);
            GetFrameRequestResult get2Result = await api.GetFrame(frame.Id, frame.FlightId);

            //Assert
            Assert.IsFalse(updateResult.HasError);
        }

        [TestMethod]
        public async Task ApiFrame_UpdateFrameResultIsSuccesfulyUpdated_Successful()
        {
            //Init
            ViewModelLocator vml = new ViewModelLocator();
            IFramesApi api = vml.FramesApi;
            api.IsTests = true;
            IFrameFactory frameFactory = vml.FrameFactoryService;
            Models.Frame frame = frameFactory.Create("#A3;232842;-81;-82;7.330;3;05;70;53.54015;14.53998;2.524;;000000;;" + Environment.NewLine);
            frame.FlightId = 0;

            //Test
            CreateFrameRequestResult createResult = await api.CreateFrame(frame);
            GetFrameRequestResult get1Result = await api.GetFrame(frame.Id, frame.FlightId);
            frame.SatelitesCount = 14;
            UpdateFrameRequestResult updateResult = await api.UpdateFrame(frame);
            GetFrameRequestResult get2Result = await api.GetFrame(frame.Id, frame.FlightId);

            //Assert
            Assert.IsTrue(updateResult.IsSuccessfulyUpdated);
        }
    }
}
