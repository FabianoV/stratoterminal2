﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StratoTerminal2.Models;
using StratoTerminal2.Models.RequestsResults;
using StratoTerminal2.Services.Contract;
using StratoTerminal2.Services.Contract.Api;
using StratoTerminal2.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StratoTerminal2.Tests.Api_Tests.Elevation_Bing_Map
{
    [TestClass]
    public class GetElevationTests
    {
        [TestMethod]
        public async Task ApiElevation_ElevationResultIsNotNull_Successful()
        {
            //Init
            ViewModelLocator vml = new ViewModelLocator();
            IBingMapApi api = vml.BingMapApi;
            api.IsTests = true;
            Location location = new Location()
            {
                Lattitude = 51,
                Longtitude = 21,
            };

            //Test
            GetElevationRequestResult result = await api.GetElevation(location);

            //Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task ApiElevation_ElevationResultHasNoError_Successful()
        {
            //Init
            ViewModelLocator vml = new ViewModelLocator();
            IBingMapApi api = vml.BingMapApi;
            api.IsTests = true;
            Location location = new Location()
            {
                Lattitude = 51,
                Longtitude = 21,
            };

            //Test
            GetElevationRequestResult result = await api.GetElevation(location);

            //Assert
            Assert.IsFalse(result.HasError);
        }

        [TestMethod]
        public async Task ApiElevation_ElevationResultHasLocationObject_Successful()
        {
            //Init
            ViewModelLocator vml = new ViewModelLocator();
            IBingMapApi api = vml.BingMapApi;
            api.IsTests = true;
            Location location = new Location()
            {
                Lattitude = 51,
                Longtitude = 21,
            };

            //Test
            GetElevationRequestResult result = await api.GetElevation(location);

            //Assert
            Assert.IsNotNull(result.Location);
        }


        [TestMethod]
        public async Task ApiElevation_ElevationResultLocationObjectHasHeight_Successful()
        {
            //Init
            ViewModelLocator vml = new ViewModelLocator();
            IBingMapApi api = vml.BingMapApi;
            api.IsTests = true;
            Location location = new Location()
            {
                Lattitude = 51,
                Longtitude = 21,
            };

            //Test
            GetElevationRequestResult result = await api.GetElevation(location);

            //Assert
            Assert.IsTrue(result.Location.Height != 0);
        }
    }
}
