﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StratoTerminal2.Models;
using StratoTerminal2.Models.RequestsResults;
using StratoTerminal2.Services.Contract;
using StratoTerminal2.Services.Contract.Api;
using StratoTerminal2.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StratoTerminal2.Tests.Api_Tests.AuthorizationAndAuthentication
{
    [TestClass]
    public class LoginTests
    {
        [TestMethod]
        public async Task ApiAuth_LoginResultIsNotNull_Successful()
        {
            //Init
            ViewModelLocator vml = new ViewModelLocator();
            IAuthApi api = vml.AuthApi;
            api.IsTests = true;
            string login = "TestQQ";
            string password = "TestTest123";

            //Test
            RegisterRequestResult resultRegister = await api.Register(login, password);
            LoginRequestResult result = await api.Login(login, password);

            //Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task ApiAuth_LoginResultIsNotNullWhenLoginFailed_Failed()
        {
            //Init
            ViewModelLocator vml = new ViewModelLocator();
            IAuthApi api = vml.AuthApi;
            api.IsTests = true;
            string login = "TestQQWW";
            string password = "TestTest123";

            //Test
            LoginRequestResult result = await api.Login(login, password);

            //Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task ApiAuth_LoginResultHasNoError_Successful()
        {
            //Init
            ViewModelLocator vml = new ViewModelLocator();
            IAuthApi api = vml.AuthApi;
            api.IsTests = true;
            string login = "TestEE";
            string password = "TestTest123";

            //Test
            RegisterRequestResult resultRegister = await api.Register(login, password);
            LoginRequestResult result = await api.Login(login, password);

            //Assert
            Assert.IsFalse(result.HasError);
        }

        [TestMethod]
        public async Task ApiAuth_LoginResultHasError_Failed()
        {
            //Init
            ViewModelLocator vml = new ViewModelLocator();
            IAuthApi api = vml.AuthApi;
            api.IsTests = true;
            string login = "TestRR";
            string password = "TestTest123";

            //Test
            LoginRequestResult result = await api.Login(login, password);

            //Assert
            Assert.IsTrue(result.HasError);
        }

        [TestMethod]
        public async Task ApiAuth_LoginResultHasUserObject_Successful()
        {
            //Init
            ViewModelLocator vml = new ViewModelLocator();
            IAuthApi api = vml.AuthApi;
            api.IsTests = true;
            string login = "TestRR";
            string password = "TestTest123";

            //Test
            RegisterRequestResult resultRegister = await api.Register(login, password);
            LoginRequestResult result = await api.Login(login, password);

            //Assert
            Assert.IsNotNull(result.User);
        }

        [TestMethod]
        public async Task ApiAuth_LoginResultUserObjectIsNUllWhenUserNotExisted_Failed()
        {
            //Init
            ViewModelLocator vml = new ViewModelLocator();
            IAuthApi api = vml.AuthApi;
            api.IsTests = true;
            string login = "TestRRRRRR";
            string password = "TestTest123";

            //Test
            RegisterRequestResult resultRegister = await api.Register(login, password);
            LoginRequestResult result = await api.Login(login, password);

            //Assert
            Assert.IsNull(result.User);
        }
    }
}
