﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StratoTerminal2.Models.RequestsResults;
using StratoTerminal2.Services.Contract;
using StratoTerminal2.Services.Contract.Api;
using StratoTerminal2.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StratoTerminal2.Tests.Api_Tests.AuthorizationAndAuthentication
{
    [TestClass]
    public class RegisterTests
    {
        [TestMethod]
        public async Task ApiAuth_RegisterResultIsNotNull_Successful()
        {
            //Init
            ViewModelLocator vml = new ViewModelLocator();
            IAuthApi api = vml.AuthApi;
            api.IsTests = true;
            string login = "Test";
            string password = "TestTest123";

            //Test
            RegisterRequestResult result = await api.Register(login, password);

            //Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task ApiAuth_RegisterResultIsNotNullWhenLoginUsed_Failed()
        {
            //Init
            ViewModelLocator vml = new ViewModelLocator();
            IAuthApi api = vml.AuthApi;
            api.IsTests = true;
            string login = "TestNN";
            string password = "TestTest123";

            //Test
            RegisterRequestResult result1 = await api.Register(login, password);
            RegisterRequestResult result2 = await api.Register(login, password);

            //Assert
            Assert.IsNotNull(result2);
        }

        [TestMethod]
        public async Task ApiAuth_RegisterResultHasNoError_Successful()
        {
            //Init
            ViewModelLocator vml = new ViewModelLocator();
            IAuthApi api = vml.AuthApi;
            api.IsTests = true;
            string login = "TestNew";
            string password = "TestTest123";

            //Test
            RegisterRequestResult result = await api.Register(login, password);

            //Assert
            Assert.IsFalse(result.HasError);
        }

        [TestMethod]
        public async Task ApiAuth_RegisterResultHasErrorWhenLoginUsed_Failed()
        {
            //Init
            ViewModelLocator vml = new ViewModelLocator();
            IAuthApi api = vml.AuthApi;
            api.IsTests = true;
            string login = "TestNewN";
            string password = "TestTest123";

            //Test
            RegisterRequestResult result1 = await api.Register(login, password);
            RegisterRequestResult result2 = await api.Register(login, password);

            //Assert
            Assert.IsTrue(result2.HasError);
        }

        [TestMethod]
        public async Task ApiAuth_RegisterResultHasSuccessfulFlag_Successful()
        {
            //Init
            ViewModelLocator vml = new ViewModelLocator();
            IAuthApi api = vml.AuthApi;
            api.IsTests = true;
            string login = "TestAAA";
            string password = "TestTest123";

            //Test
            RegisterRequestResult result = await api.Register(login, password);

            //Assert
            Assert.IsTrue(result.IsSuccessful);
        }

        [TestMethod]
        public async Task ApiAuth_RegisterResultHasSuccessfulFlagFalseWhenUserExisted_Failed()
        {
            //Init
            ViewModelLocator vml = new ViewModelLocator();
            IAuthApi api = vml.AuthApi;
            api.IsTests = true;
            string login = "TestZ";
            string password = "TestTest123";

            //Test
            RegisterRequestResult result1 = await api.Register(login, password);
            RegisterRequestResult result2 = await api.Register(login, password);

            //Assert
            Assert.IsFalse(result2.IsSuccessful);
        }

        [TestMethod]
        public async Task ApiAuth_RegisterResultHasUserObject_Successful()
        {
            //Init
            ViewModelLocator vml = new ViewModelLocator();
            IAuthApi api = vml.AuthApi;
            api.IsTests = true;
            string login = "TestAAAA";
            string password = "TestTest123";

            //Test
            RegisterRequestResult result = await api.Register(login, password);

            //Assert
            Assert.IsNotNull(result.User);
        }

        [TestMethod]
        public async Task ApiAuth_RegisterResultNoHasUserObjectWhenUserExisted_Failed()
        {
            //Init
            ViewModelLocator vml = new ViewModelLocator();
            IAuthApi api = vml.AuthApi;
            api.IsTests = true;
            string login = "TestZAAA";
            string password = "TestTest123";

            //Test
            RegisterRequestResult result1 = await api.Register(login, password);
            RegisterRequestResult result2 = await api.Register(login, password);

            //Assert
            Assert.IsNull(result2.User);
        }
    }
}
