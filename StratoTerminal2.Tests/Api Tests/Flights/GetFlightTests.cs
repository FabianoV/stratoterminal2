﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StratoTerminal2.Models;
using StratoTerminal2.Models.RequestsResults;
using StratoTerminal2.Services.Contract;
using StratoTerminal2.Services.Contract.Api;
using StratoTerminal2.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StratoTerminal2.Tests.Api_Tests.Flights
{
    [TestClass]
    public class GetFlightTests
    {
        [TestMethod]
        public async Task ApiFlight_GetFlightResultIsNotNull_Successful()
        {
            //Init
            ViewModelLocator vml = new ViewModelLocator();
            IFlightsApi api = vml.FlightsApi;
            api.IsTests = true;
            IFlightFactory flightFactory = vml.FlightFactory;
            Flight flight = await flightFactory.CreateFlight("WPF Tests " + Guid.NewGuid(), "A", "ST2.0", "Unit Tests", true);

            //Test
            CreateFlightRequestResult createResult = await api.CreateFlight(flight);
            GetFlighRequestResult result = await api.GetFlight(createResult.Flight.Id);

            //Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task ApiFlight_GetFlightResultObjectIsNotNull_Successful()
        {
            //Init
            ViewModelLocator vml = new ViewModelLocator();
            IFlightsApi api = vml.FlightsApi;
            api.IsTests = true;
            IFlightFactory flightFactory = vml.FlightFactory;
            Flight flight = await flightFactory.CreateFlight("WPF Tests " + Guid.NewGuid(), "A", "ST2.0", "Unit Tests", true);

            //Test
            CreateFlightRequestResult createResult = await api.CreateFlight(flight);
            GetFlighRequestResult result = await api.GetFlight(createResult.Flight.Id);

            //Assert
            Assert.IsNotNull(result.Flight);
        }

        [TestMethod]
        public async Task ApiFlight_GetFlightResultNoHasError_Successful()
        {
            //Init
            ViewModelLocator vml = new ViewModelLocator();
            IFlightsApi api = vml.FlightsApi;
            api.IsTests = true;
            IFlightFactory flightFactory = vml.FlightFactory;
            Flight flight = await flightFactory.CreateFlight("WPF Tests " + Guid.NewGuid(), "A", "ST2.0", "Unit Tests", true);

            //Test
            CreateFlightRequestResult createResult = await api.CreateFlight(flight);
            GetFlighRequestResult result = await api.GetFlight(createResult.Flight.Id);

            //Assert
            Assert.IsFalse(result.HasError);
        }
    }
}
