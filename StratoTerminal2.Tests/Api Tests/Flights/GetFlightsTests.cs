﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StratoTerminal2.Models;
using StratoTerminal2.Models.RequestsResults;
using StratoTerminal2.Services.Contract;
using StratoTerminal2.Services.Contract.Api;
using StratoTerminal2.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StratoTerminal2.Tests.Api_Tests.Flights
{
    [TestClass]
    public class GetFlightsTests
    {
        [TestMethod]
        public async Task ApiFlight_GetFlightsResultIsNotNull_Successful()
        {
            //Init
            ViewModelLocator vml = new ViewModelLocator();
            IFlightsApi api = vml.FlightsApi;
            api.IsTests = true;

            //Test
            GetFlightsRequestResult result = await api.GetFlights();

            //Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task ApiFlight_GetFlightsResultCollectionIsNotNull_Successful()
        {
            //Init
            ViewModelLocator vml = new ViewModelLocator();
            IFlightsApi api = vml.FlightsApi;
            api.IsTests = true;
            IFlightFactory flightFactory = vml.FlightFactory;
            Flight flight = await flightFactory.CreateFlight("WPF Tests " + Guid.NewGuid(), "A", "ST2.0", "Unit Tests", true);

            //Test
            CreateFlightRequestResult createResult = await api.CreateFlight(flight);
            GetFlightsRequestResult result = await api.GetFlights();

            //Assert
            Assert.IsNotNull(result.Flights);
        }

        [TestMethod]
        public async Task ApiFlight_GetFlightsResultCollectionHasElements_Successful()
        {
            //Init
            ViewModelLocator vml = new ViewModelLocator();
            IFlightsApi api = vml.FlightsApi;
            api.IsTests = true;
            IFlightFactory flightFactory = vml.FlightFactory;
            Flight flight = await flightFactory.CreateFlight("WPF Tests " + Guid.NewGuid(), "A", "ST2.0", "Unit Tests", true);

            //Test
            CreateFlightRequestResult createResult = await api.CreateFlight(flight);
            GetFlightsRequestResult result = await api.GetFlights();

            //Assert
            Assert.IsTrue(result.Flights.Count > 0);
        }

        [TestMethod]
        public async Task ApiFlight_GetFlightsResultHasNoError_Successful()
        {
            //Init
            ViewModelLocator vml = new ViewModelLocator();
            IFlightsApi api = vml.FlightsApi;
            api.IsTests = true;
            IFlightFactory flightFactory = vml.FlightFactory;
            Flight flight = await flightFactory.CreateFlight("WPF Tests " + Guid.NewGuid(), "A", "ST2.0", "Unit Tests", true);

            //Test
            CreateFlightRequestResult createResult = await api.CreateFlight(flight);
            GetFlightsRequestResult result = await api.GetFlights();

            //Assert
            Assert.IsFalse(result.HasError);
        }
    }
}
