﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StratoTerminal2.Models;
using StratoTerminal2.Models.AzureModels;
using StratoTerminal2.Models.RequestsResults;
using StratoTerminal2.Services.Contract;
using StratoTerminal2.Services.Contract.Api;
using StratoTerminal2.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StratoTerminal2.Tests.Api_Tests.Flights
{
    [TestClass]
    public class UpdateFlightTests
    {
        //[TestMethod]
        //public async Task ApiFlight_UpdateFlightResultIsNotNull_Successful()
        //{
        //    //Init
        //    ViewModelLocator vml = new ViewModelLocator();
        //    IFlightsApi api = vml.FlightsApi;
        //    api.IsTests = true;
        //    IFlightFactory flightFactory = vml.FlightFactory;
        //    Flight flight = await flightFactory.CreateFlight("WPF Tests " + Guid.NewGuid(), "A", "ST2.0", "Unit Tests", true);

        //    //Test
        //    CreateFlightRequestResult createResult = await api.CreateFlight(flight);
        //    FlightAzure flightAzure = createResult.Flight;
        //    GetFlighRequestResult get1Result = await api.GetFlight(flight.Id);
        //    flightAzure.DeviceName = "B";
        //    UpdateFlightRequestResult updateResult = await api.UpdateFlight(flight);
        //    GetFlighRequestResult get2Result = await api.GetFlight(flight.Id);

        //    //Assert
        //    Assert.IsNotNull(updateResult);
        //}

        //[TestMethod]
        //public async Task ApiFlight_UpdateFlightResultHasNoError_Successful()
        //{
        //    //Init
        //    ViewModelLocator vml = new ViewModelLocator();
        //    IFlightsApi api = vml.FlightsApi;
        //    api.IsTests = true;
        //    IFlightFactory flightFactory = vml.FlightFactory;
        //    Flight flight = await flightFactory.CreateFlight("WPF Tests " + Guid.NewGuid(), "A", "ST2.0", "Unit Tests", true);

        //    //Test
        //    CreateFlightRequestResult createResult = await api.CreateFlight(flight);
        //    flight = createResult.Flight;
        //    GetFlighRequestResult get1Result = await api.GetFlight(flight.Id);
        //    flight.DeviceName = "B";
        //    UpdateFlightRequestResult updateResult = await api.UpdateFlight(flight);
        //    GetFlighRequestResult get2Result = await api.GetFlight(flight.Id);

        //    //Assert
        //    Assert.IsFalse(updateResult.HasError);
        //}

        //[TestMethod]
        //public async Task ApiFlight_UpdateFlightResultIsSuccesfulyUpdated_Successful()
        //{
        //    //Init
        //    ViewModelLocator vml = new ViewModelLocator();
        //    IFlightsApi api = vml.FlightsApi;
        //    api.IsTests = true;
        //    IFlightFactory flightFactory = vml.FlightFactory;
        //    Flight flight = await flightFactory.CreateFlight("WPF Tests " + Guid.NewGuid(), "A", "ST2.0", "Unit Tests", true);

        //    //Test
        //    CreateFlightRequestResult createResult = await api.CreateFlight(flight);
        //    flight = createResult.Flight;
        //    GetFlighRequestResult get1Result = await api.GetFlight(flight.Id);
        //    flight.DeviceName = "B";
        //    UpdateFlightRequestResult updateResult = await api.UpdateFlight(flight);
        //    GetFlighRequestResult get2Result = await api.GetFlight(flight.Id);

        //    //Assert
        //    Assert.IsTrue(updateResult.IsSuccessfulyUpdated);
        //}

        //[TestMethod]
        //public async Task ApiFlight_UpdateFlightResultObjectDataUpdated_Successful()
        //{
        //    //Init
        //    ViewModelLocator vml = new ViewModelLocator();
        //    IFlightsApi api = vml.FlightsApi;
        //    api.IsTests = true;
        //    IFlightFactory flightFactory = vml.FlightFactory;
        //    Flight flight = await flightFactory.CreateFlight("WPF Tests " + Guid.NewGuid(), "A", "ST2.0", "Unit Tests", true);

        //    //Test
        //    CreateFlightRequestResult createResult = await api.CreateFlight(flight);
        //    flight = createResult.Flight;
        //    GetFlighRequestResult get1Result = await api.GetFlight(flight.Id);
        //    flight.DeviceName = "B";
        //    UpdateFlightRequestResult updateResult = await api.UpdateFlight(flight);
        //    GetFlighRequestResult get2Result = await api.GetFlight(flight.Id);

        //    //Assert
        //    Assert.IsTrue(get2Result.Flight.DeviceName == "B");
        //}
    }
}
