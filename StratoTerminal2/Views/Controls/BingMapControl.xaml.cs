﻿using GalaSoft.MvvmLight.Ioc;
using Microsoft.Maps.MapControl.WPF;
using StratoTerminal2.Models.RequestsResults;
using StratoTerminal2.Services.Contract;
using StratoTerminal2.Services.Contract.Api;
using StratoTerminal2.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace StratoTerminal2.Views.Controls
{
    /// <summary>
    /// Interaction logic for MapControl.xaml
    /// </summary>
    public partial class BingMapControl : UserControl
    {
        public BingMapControl()
        {
            InitializeComponent();
            //this.DataContext = new MapControl();
        }

        private void Map_MouseMove(object sender, MouseEventArgs e)
        {
            //Get the mouse click coordinates
            Point mousePosition = e.GetPosition(this);

            //Convert the mouse coordinates to a locatoin on the map
            Location location = Map.ViewportPointToLocation(mousePosition);

            //this.LattitudeInfo.Text = location.Latitude.ToString();
            //this.LongtitudeInfo.Text = location.Longitude.ToString();
        }

        private async void Map_MouseDown(object sender, MouseButtonEventArgs e)
        {
            //Get the mouse click coordinates
            Point mousePosition = e.GetPosition(this);

            //Convert the mouse coordinates to a locatoin on the map
            Location location = Map.ViewportPointToLocation(mousePosition);
            Models.Location STLocation = new Models.Location()
            {
                Lattitude = location.Latitude,
                Longtitude = location.Longitude,
            };

            IBingMapApi apiService = SimpleIoc.Default.GetInstance<IBingMapApi>();
            GetElevationRequestResult result = await apiService.GetElevation(STLocation);

            if (result.HeightFound)
            {
                //this.HeightInfo.Text = result.Location.Height.ToString();
            }
        }

        private void MapAddHeight_Click(object sender, RoutedEventArgs e)
        {
            this.Map.ZoomLevel++;
        }

        private void MapSubHeight_Click(object sender, RoutedEventArgs e)
        {
            this.Map.ZoomLevel--;
        }

        #region Ballon Path Drawing
        public ViewModel.Map MapViewModel { get { return this.DataContext as ViewModel.Map; }  }
        private DispatcherTimer pathTimer;
        private void Map_Loaded(object sender, RoutedEventArgs e)
        {
            this.pathTimer = new DispatcherTimer();
            this.pathTimer.Interval = TimeSpan.FromSeconds(1);
            this.pathTimer.Tick += PathTimer_Tick;
            this.pathTimer.Start();

            //Add cache layer
            //MapTileLayer tileLayer = new MapTileLayer()
            //{
            //    TileSource = new LocalTileSource(@"SouthEndUniversityHospital\{quadkey}.png")
            //};
            //this.Map.Children.Add(tileLayer);
        }
        private void PathTimer_Tick(object sender, EventArgs e)
        {
            try
            {
                if (this.BallonPath.Locations == null)
                {
                    this.BallonPath.Locations = new LocationCollection();
                }

                this.BallonPath.Locations.Clear();

                lock (MapViewModel.Locations)
                {
                    foreach (Models.Location location in MapViewModel.Locations)
                    {
                        Microsoft.Maps.MapControl.WPF.Location locationMap = new Location()
                        {
                            Latitude = location.Lattitude,
                            Longitude = location.Longtitude,
                            Altitude = location.Height,
                            AltitudeReference = AltitudeReference.Ground,
                        };
                        this.BallonPath.Locations.Add(locationMap);
                    }
                }

                if (isCentered)
                {
                    this.CenterDevice_Click(this, new RoutedEventArgs());
                }
            }
            catch (Exception)
            {
                // Do nothing and wait to next refresh
            }
        }
        #endregion

        private void CenterDevice_Click(object sender, RoutedEventArgs e)
        {
            this.Map.Center = this.BallonPosition.Location;
        }

        private bool isCentered = false;
        private void BlockCenterDevice_Click(object sender, RoutedEventArgs e)
        {
            if (isCentered)
            {
                isCentered = false;

                BitmapImage center_off_image = new BitmapImage();
                center_off_image.BeginInit();
                center_off_image.UriSource = new Uri("/StratoTerminal2;component/Content/center_off.png", UriKind.RelativeOrAbsolute);
                center_off_image.EndInit();
                this.BlockCenterDeviceImage.Source = center_off_image;
            }
            else
            {
                isCentered = true;

                BitmapImage center_on_image = new BitmapImage();
                center_on_image.BeginInit();
                center_on_image.UriSource = new Uri("/StratoTerminal2;component/Content/center_on.png", UriKind.RelativeOrAbsolute);
                center_on_image.EndInit();
                this.BlockCenterDeviceImage.Source = center_on_image;
            }
        }
    }
}
