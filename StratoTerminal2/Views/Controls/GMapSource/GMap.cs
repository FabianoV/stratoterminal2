﻿using System.Windows.Controls;
using System.Windows.Media;
using GMap.NET.WindowsPresentation;
using System.Globalization;
using System.Windows;
using System;
using Library = GMap.NET.WindowsPresentation;

namespace StratoTerminal2.Views.Controls.GMapSource
{
    public class Map : Library.GMapControl
    {
        #region Draw Statistics On Map When Debug
#if DEBUG
        public long ElapsedMilliseconds;

        DateTime start;
        DateTime end;
        int delta;

        private int counter;
        readonly Typeface tf = new Typeface("GenericSansSerif");
        readonly System.Windows.FlowDirection fd = new System.Windows.FlowDirection();

        protected override void OnRender(DrawingContext drawingContext)
        {
            start = DateTime.Now;

            base.OnRender(drawingContext);

            end = DateTime.Now;
            delta = (int)(end - start).TotalMilliseconds;

            FormattedText text = new FormattedText(string.Format(CultureInfo.InvariantCulture, "{0:0.0}", Zoom) + "z, " + MapProvider + ", refresh: " + counter++ + ", load: " + ElapsedMilliseconds + "ms, render: " + delta + "ms", CultureInfo.InvariantCulture, fd, tf, 20, Brushes.Blue);
            drawingContext.DrawText(text, new Point(text.Height, text.Height));
            text = null;
        }
#endif
        #endregion
    }
}
