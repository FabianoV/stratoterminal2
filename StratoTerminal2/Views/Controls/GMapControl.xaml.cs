﻿using GalaSoft.MvvmLight.Ioc;
using GMap.NET;
using GMap.NET.MapProviders;
using GMap.NET.WindowsPresentation;
using StratoTerminal2.Models;
using StratoTerminal2.Services.Contract;
using StratoTerminal2.Tools.GMap.Extension;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace StratoTerminal2.Views.Controls
{
    /// <summary>
    /// Interaction logic for GMapControl.xaml
    /// </summary>
    public partial class GMapControl : UserControl
    {
        //Fields
        private CustomGMapRoute ballonPath;
        private DispatcherTimer pathTimer;
        private bool isCentered = false;

        //Properties
        public ViewModel.Map MapViewModel { get { return this.DataContext as ViewModel.Map; } }
        public ViewModel.Controls.GMapControl GMapControlViewModel { get; set; }

        //Constructor
        public GMapControl()
        {
            InitializeComponent();
            this.GMapControlViewModel = new ViewModel.Controls.GMapControl(SimpleIoc.Default.GetInstance<IFrameHolder>());
        }

        //Methods
        private void Map_Loaded(object sender, RoutedEventArgs e)
        {
            this.Map.MapProvider = GMapProviders.GoogleHybridMap;
            this.Map.Position = new PointLatLng(52.037049, 19.517532);
            this.Map.Zoom = 7;
            this.Map.ScaleMode = ScaleModes.Dynamic;
            this.Map.Manager.Mode = AccessMode.ServerAndCache;

            // polyline - ballon path
            this.ballonPath = new CustomGMapRoute(new List<PointLatLng>());
            this.ballonPath.RegenerateShape(this.Map);
            this.Map.Markers.Add(this.ballonPath);
            this.ballonPath.Shape = new Ellipse
            {
                Width = 10,
                Height = 10,
                Stroke = Brushes.Black,
                StrokeThickness = 1.5
            };
            //(this.ballonPath.Shape as Path).Stroke = new SolidColorBrush(Colors.Red);
            //(this.ballonPath.Shape as Path).StrokeThickness = 20;
            this.pathTimer = new DispatcherTimer();
            this.pathTimer.Interval = TimeSpan.FromSeconds(1);
            this.pathTimer.Tick += PathTimer_Tick;
            this.pathTimer.Start();

            // map events
            this.Map.OnPositionChanged += new PositionChanged(Map_OnCurrentPositionChanged);
            this.Map.OnTileLoadComplete += new TileLoadComplete(Map_OnTileLoadComplete);
            this.Map.OnTileLoadStart += new TileLoadStart(Map_OnTileLoadStart);
            this.Map.OnMapTypeChanged += new MapTypeChanged(Map_OnMapTypeChanged);
            this.Map.MouseMove += new System.Windows.Input.MouseEventHandler(Map_MouseMove);
            this.Map.MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(Map_MouseLeftButtonDown);
            this.Map.MouseEnter += new MouseEventHandler(Map_MouseEnter);
        }

        private void PathTimer_Tick(object sender, EventArgs e)
        {
            try
            {
                this.ballonPath.Points.Clear();

                lock (MapViewModel.Locations)
                {
                    foreach (Models.Location location in MapViewModel.Locations)
                    {
                        PointLatLng point = new PointLatLng(location.Lattitude, location.Longtitude);
                        this.ballonPath.Points.Add(point);
                    }
                }

                if (isCentered)
                {
                    this.CenterDevice_Click(this, new RoutedEventArgs());
                }
            }
            catch (Exception)
            {
                // Do nothing and wait to next refresh
            }
        }

        private void Map_MouseEnter(object sender, MouseEventArgs e)
        {
        }

        private void Map_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
        }

        private void Map_MouseMove(object sender, MouseEventArgs e)
        {
        }

        private void Map_OnMapTypeChanged(GMapProvider type)
        {
        }

        private void Map_OnTileLoadStart()
        {
        }

        private void Map_OnTileLoadComplete(long ElapsedMilliseconds)
        {
        }

        private void Map_OnCurrentPositionChanged(PointLatLng point)
        {
        }

        private void MapAddHeight_Click(object sender, RoutedEventArgs e)
        {
            this.Map.Zoom++;
        }

        private void MapSubHeight_Click(object sender, RoutedEventArgs e)
        {
            this.Map.Zoom--;
        }

        private void BlockCenterDevice_Click(object sender, RoutedEventArgs e)
        {
            if (isCentered)
            {
                isCentered = false;

                BitmapImage center_off_image = new BitmapImage();
                center_off_image.BeginInit();
                center_off_image.UriSource = new Uri("/StratoTerminal2;component/Content/center_off.png", UriKind.RelativeOrAbsolute);
                center_off_image.EndInit();
                this.BlockCenterDeviceImage.Source = center_off_image;
            }
            else
            {
                isCentered = true;

                BitmapImage center_on_image = new BitmapImage();
                center_on_image.BeginInit();
                center_on_image.UriSource = new Uri("/StratoTerminal2;component/Content/center_on.png", UriKind.RelativeOrAbsolute);
                center_on_image.EndInit();
                this.BlockCenterDeviceImage.Source = center_on_image;
            }
        }

        private void CenterDevice_Click(object sender, RoutedEventArgs e)
        {
            this.Map.Position = new PointLatLng()
            {
                Lat = this.GMapControlViewModel.BallonPosition.Lattitude,
                Lng = this.GMapControlViewModel.BallonPosition.Longtitude,
            };
        }
    }
}
