﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Ioc;
using StratoTerminal2.Services.Contract;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StratoTerminal2.ViewModel
{
    public class SettingsPanel : ViewModelBase
    {
        //Services
        public ICOMPortConfig DataListenerService { get; set; }
        public IFlightManager FlightManager { get; set; }
        public IMessageBoxService MessageBoxService { get; set; }

        //Properties
        public ObservableCollection<string> ComPorts { get; set; }
        public ObservableCollection<int> BaudRates { get; set; }
        public string SelectedComPort { get; set; }
        public int SelectedBaudRate { get; set; }
        public bool IsPortOpen { get { return this.DataListenerService.IsPortOpen; } }

        //Commands
        public RelayCommand OpenPortCommand { get; set; }

        //Constructor
        public SettingsPanel(ICOMPortConfig dataListenerService, IFlightManager flightManager, IMessageBoxService messageBoxService)
        {
            //Services
            this.DataListenerService = dataListenerService;
            this.FlightManager = flightManager;
            this.MessageBoxService = messageBoxService;

            //Init
            this.ComPorts = new ObservableCollection<string>(this.DataListenerService.GetComPortsNames());
            this.BaudRates = new ObservableCollection<int>()
            {
                600, 1200, 2400, 4800, 9600, 14400, 19200, 28800, 38400, 56000, 57600, 115200, 128000, 256000,
            };
            this.SelectedComPort = this.ComPorts[0];
            this.SelectedBaudRate = 57600;

            //Commands
            this.OpenPortCommand = new RelayCommand(OpenPort);
        }

        //Methods
        public void OpenPort()
        {
            //Check is flight is choosen
            if (this.FlightManager.SelectedFlight == null)
            {
                this.MessageBoxService.Show("First select your flight!");
                return;
            }

            //Open Port
            if (this.DataListenerService.Port == null)
            {
                this.DataListenerService.CreatePort(this.SelectedComPort, this.SelectedBaudRate);
            }
            else if (!this.DataListenerService.IsPortOpen)
            {
                this.DataListenerService.CreatePort(this.SelectedComPort, this.SelectedBaudRate);
            }
            else
            {
                this.DataListenerService.ClosePort();
            }

            this.RaisePropertyChanged("IsPortOpen");
        }

        public void UpdateComPorts()
        {
            List<string> newPortNames = this.DataListenerService.GetComPortsNames();

            if (newPortNames.Contains(this.SelectedComPort))
            {
                //Remove elements without selected port
                foreach (string port in this.ComPorts.ToList())
                {
                    if (port != this.SelectedComPort)
                    {
                        this.ComPorts.Remove(port);
                    }
                }

                //Add all ports without selected
                foreach (string port in newPortNames)
                {
                    if (port != this.SelectedComPort)
                    {
                        this.ComPorts.Add(port);
                    }
                }

                //And old selected port is now first on the list and is active
                this.RaisePropertyChanged(nameof(ComPorts));
            }
            else
            {
                //Remove all ports
                this.ComPorts.Clear();

                //Add all new ports
                foreach (string port in newPortNames)
                {
                    this.ComPorts.Add(port);
                }

                //Refresh UI and select first port
                this.RaisePropertyChanged(nameof(ComPorts));
                this.SelectedComPort = this.ComPorts[0];
                this.RaisePropertyChanged(nameof(SelectedComPort));
            }
        }
    }
}
