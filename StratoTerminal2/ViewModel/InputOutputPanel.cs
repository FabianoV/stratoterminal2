﻿using GalaSoft.MvvmLight;
using StratoTerminal2.Models;
using StratoTerminal2.Services.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StratoTerminal2.ViewModel
{
    public class InputOutputPanel : ViewModelBase
    {
        //Services
        public ICOMPortConfig DataListenerService { get; set; }
        public IFrameFactory FrameFactory { get; set; }
        public IFrameHolder FrameHolder { get; set; }

        //Properties
        public Frame LastFrame { get { return FrameHolder.LastFrame; } }
        public string IOControl { get { if (this.LastFrame != null) { return this.LastFrame.IOControl; } else { return ""; } } }

        public bool IsFirstInOutVisible { get { return this.IOControl.Length >= 2; } }
        public bool FirstInValue { get { if (IOControl.Length >= 1) { return this.IOControl[0] != '0';  } else { return false; } } }
        public bool FirstOutValue { get { if (IOControl.Length >= 2) { return this.IOControl[1] != '0';  } else { return false; } } }

        public bool IsSecondInOutVisible { get { return this.IOControl.Length >= 4; } }
        public bool SecondInValue { get { if (IOControl.Length >= 3) { return this.IOControl[2] != '0'; } else { return false; } } }
        public bool SecondOutValue { get { if (IOControl.Length >= 4) { return this.IOControl[3] != '0'; } else { return false; } } }

        public bool IsThirdInOutVisible { get { return this.IOControl.Length >= 6; } }
        public bool ThirdInValue { get { if (IOControl.Length >= 5) { return this.IOControl[4] != '0'; } else { return false; } } }
        public bool ThirdOutValue { get { if (IOControl.Length >= 6) { return this.IOControl[5] != '0'; } else { return false; } } }

        public bool IsFourthInOutVisible { get { return this.IOControl.Length >= 8; } }
        public bool FourthInValue { get { if (IOControl.Length >= 7) { return this.IOControl[6] != '0'; } else { return false; } } }
        public bool FourthOutValue { get { if (IOControl.Length >= 8) { return this.IOControl[7] != '0'; } else { return false; } } }

        //Constructor
        public InputOutputPanel(ICOMPortConfig dataListenerService, IFrameHolder frameHolderService, IFrameFactory frameFactory)
        {
            //Services
            this.DataListenerService = dataListenerService;
            this.FrameHolder = frameHolderService;
            this.FrameFactory = frameFactory;

            //Init
            this.FrameFactory.FrameCreated += FrameFactory_FrameCreated;
        }

        //Methods
        private void FrameFactory_FrameCreated(object sender, Models.EventArgs.FrameCreatedEventArgs e)
        {
            RaisePropertyChanged("IsFirstInOutVisible");
            RaisePropertyChanged("FirstInValue");
            RaisePropertyChanged("FirstOutValue");

            RaisePropertyChanged("IsSecondInOutVisible");
            RaisePropertyChanged("SecondInValue");
            RaisePropertyChanged("SecondOutValue");

            RaisePropertyChanged("IsThirdInOutVisible");
            RaisePropertyChanged("ThirdInValue");
            RaisePropertyChanged("ThirdOutValue");

            RaisePropertyChanged("IsFourthInOutVisible");
            RaisePropertyChanged("FourthInValue");
            RaisePropertyChanged("FourthOutValue");
        }
    }
}
