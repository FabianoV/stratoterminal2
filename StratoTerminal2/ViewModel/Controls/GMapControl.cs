﻿using GalaSoft.MvvmLight;
using StratoTerminal2.Models;
using StratoTerminal2.Services.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StratoTerminal2.ViewModel.Controls
{
    public class GMapControl : ViewModelBase
    {
        //Services
        public IFrameHolder FrameHolder { get; set; }

        //Properties
        public Location BallonPosition
        {
            get
            {
                if (this.FrameHolder.LastFrame != null && (this.FrameHolder.LastFrame.Fix == 2 || this.FrameHolder.LastFrame.Fix == 3))
                {
                    Location location = new Location()
                    {
                        Lattitude = this.FrameHolder.LastFrame.Lattitude,
                        Longtitude = this.FrameHolder.LastFrame.Longtitude,
                        Height = this.FrameHolder.LastFrame.Height,
                    };


                    return location;
                }
                else
                {
                    return new Location();
                }
            }
        }

        //Commands

        //Constructor
        public GMapControl(IFrameHolder frameHolder)
        {
            //Services
            this.FrameHolder = frameHolder;

            //Initialize
        }

        //Methods
    }
}
