﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using StratoTerminal2.Services.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StratoTerminal2.Models.CommandsObjects;

namespace StratoTerminal2.ViewModel
{
    public class Terminal : ViewModelBase
    {
        //Services
        public ICOMPortConfig DataListener { get; set; }
        public IMessageBoxService MessageBoxService { get; set; }
        public IFrameFactory FrameFactory { get; set; }
        public ICommandManagementService CommandManagementService { get; set; }

        //Properties
        public bool IsAutoScroll { get; set; }
        public bool IsSendCR { get; set; }
        public bool IsSendAndClear { get; set; }
        public bool IsLogData { get; set; }
        public bool IsSendToAzure { get; set; }
        public string SendText { get; set; }
        public string Telemetry { get; set; }

        public List<Models.CommandsObjects.GroupData> CommandsGroups { get { return this.CommandManagementService.Groups; } }

        //Visibility properties
        public bool IsCommandPanelVisible { get; set; }

        //Commands
        public RelayCommand SendCommand { get; set; }
        public RelayCommand SwitchCommandPanelCommand { get; set; }
        public RelayCommand<string> WriteCommandCommand { get; set; }

        //Constructor
        public Terminal(ICOMPortConfig dataListener, IMessageBoxService messageBoxService, IFrameFactory frameFactory, 
            ICommandManagementService commandManagementService)
        {
            //Services
            this.DataListener = dataListener;
            this.MessageBoxService = messageBoxService;
            this.FrameFactory = frameFactory;
            this.CommandManagementService = commandManagementService;

            //Init
            this.SendText = "";
            this.Telemetry = "";
            this.IsAutoScroll = true;
            this.IsSendAndClear = true;
            this.IsSendCR = true;
            this.IsLogData = true;

            this.FrameFactory.FrameCreated += FrameFactory_FrameCreated;

            //Commands
            this.SendCommand = new RelayCommand(Send);
            this.SwitchCommandPanelCommand = new RelayCommand(SwitchCommandPanel);
            this.WriteCommandCommand = new RelayCommand<string>(WriteCommand);
        }

        //Methods
        public void Send()
        {
            if (this.DataListener.IsPortOpen) // sprawdz czy port juz otwarty
            {
                try
                {
                    this.DataListener.Port.Write(this.SendText); // wyslij string

                    if (this.IsSendCR)
                    {
                        this.DataListener.Port.Write("\r"); // dodaj znak CR
                    }
                }
                catch (Exception ex)
                {
                    //TODO: log and message box
                    //MessageBox.Show("Katastrofa! Treść błędu: " + Environment.NewLine + ex);
                }

                if (this.IsSendAndClear)
                {
                    this.SendText = "";  // czysc okienko wysylania po nadaniu stringa
                    this.RaisePropertyChanged("SendText");
                }
            }
            else
            {
                this.MessageBoxService.Show("Nie można wysłać wiadomości, ponieważ port jest zamknięty.");
            }
        }

        public void SwitchCommandPanel()
        {
            if (this.IsCommandPanelVisible)
            {
                this.IsCommandPanelVisible = false;
            }
            else
            {
                this.IsCommandPanelVisible = true;
            }

            this.RaisePropertyChanged("IsCommandPanelVisible");
        }

        public void WriteCommand(string command)
        {
            //put into terminal textbox clicked command
            this.SendText = command;
            this.RaisePropertyChanged("SendText");

            //hide commands panel
            this.IsCommandPanelVisible = false;
            this.RaisePropertyChanged("IsCommandPanelVisible");
        }

        private void FrameFactory_FrameCreated(object sender, Models.EventArgs.FrameCreatedEventArgs e)
        {
            this.Telemetry += e.NewFrame.Raw;
            this.RaisePropertyChanged("Telemetry");
        }
    }
}
