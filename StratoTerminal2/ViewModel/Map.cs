﻿using GalaSoft.MvvmLight;
using StratoTerminal2.Models;
using StratoTerminal2.Models.Enums;
using StratoTerminal2.Models.EventArgs;
using StratoTerminal2.Services.Contract;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace StratoTerminal2.ViewModel
{
    public class Map : ViewModelBase
    {
        //Service
        public IFrameFactory FrameFactory { get; set; }
        public IFrameHolder FrameHolder { get; set; }

        //Binding Properties
        public ObservableCollection<Location> Locations { get; set; }

        public double BallonLattitude { get; set; }
        public double BallonLongtitude { get; set; }

        public string BallonLocation { get { return string.Format("{0},{1}", this.BallonLattitude.ToString().Replace(",", "."), BallonLongtitude.ToString().Replace(",", "."));} }

        public double Longtitude { get; set; }

        //Constructor
        public Map(IFrameFactory frameFactory, IFrameHolder frameHolder)
        {
            //Services
            this.FrameFactory = frameFactory;
            this.FrameHolder = frameHolder;

            //Initialize
            this.Longtitude = 10;
            this.Locations = new ObservableCollection<Location>();

            this.FrameFactory.FrameCreated += FrameFactory_FrameCreated;
        }

        //Methods
        private void FrameFactory_FrameCreated(object sender, FrameCreatedEventArgs e)
        {
            
            if (e.NewFrame != null &&
                !double.IsNaN(e.NewFrame.Lattitude) &&
                !double.IsNaN(e.NewFrame.Longtitude) &&
                e.NewFrame.State == FrameState.Data)
            {
                Location location = new Location()
                {
                    Lattitude = e.NewFrame.Lattitude,
                    Longtitude = e.NewFrame.Longtitude,
                    Height = e.NewFrame.Height,
                };
                this.Locations.Add(location);

                this.BallonLattitude = e.NewFrame.Lattitude;
                this.BallonLongtitude = e.NewFrame.Longtitude;
                RaisePropertyChanged("BallonLocation");
            }
        }
    }
}
