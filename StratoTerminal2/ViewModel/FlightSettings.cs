﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using RoyT.TimePicker;
using StratoTerminal2.Models;
using StratoTerminal2.Models.AzureModels;
using StratoTerminal2.Models.RequestsResults;
using StratoTerminal2.Services.Contract;
using StratoTerminal2.Services.Contract.Api;
using StratoTerminal2.Tools;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StratoTerminal2.ViewModel
{
    public class FlightSettings : ViewModelBase
    {
        //Services
        public IFlightsApi FlightsApi { get; set; }
        public IFlightManager FlightManager { get; set; }
        public IMessageBoxService MessageBoxService { get; set; }
        public IInternetConectionService InternetConnectionService { get; set; }

        //Fields
        private Flight selectedFlight;

        //Binding Properties
        public Flight PanelFlight { get; set; }
        public Flight GlobalSelectedFlight { get; set; }
        public ObservableCollection<Flight> Flights { get; set; }
        public Flight SelectedFlight
        {
            get { return selectedFlight; }
            set
            {
                this.selectedFlight = value;
                this.PanelFlight = value.Clone();
                this.RaisePropertyChanged("SelectedFlight");
                this.RaisePropertyChanged("PanelFlight");
            }
        }

        //Time Picker Settings
        public DigitalTime MaxTimePicker { get; set; }
        public DigitalTime MinTimePicker { get; set; }

        //Commands
        public RelayCommand FlightSettingsLoadedCommand { get; set; }
        public RelayCommand CreateFlightCommand { get; set; }
        public RelayCommand UpdateFlightCommand { get; set; }
        public RelayCommand ClearCommand { get; set; }
        public RelayCommand DeleteFlightCommand { get; set; }
        public RelayCommand SelectGlobalFlightCommand { get; set; }
        public RelayCommand RefreshFlightsCommand { get; set; }

        //Constructor
        public FlightSettings(IFlightsApi flightsApi, IFlightManager flightManager, IMessageBoxService messageBoxService, 
            IInternetConectionService internetConnectionService)
        {
            //Services
            this.FlightsApi = flightsApi;
            this.FlightManager = flightManager;
            this.MessageBoxService = messageBoxService;
            this.InternetConnectionService = internetConnectionService;

            //Commands
            this.FlightSettingsLoadedCommand = new RelayCommand(FlightSettingsLoaded);
            this.CreateFlightCommand = new RelayCommand(CreateFlight);
            this.UpdateFlightCommand = new RelayCommand(UpdateFlight);
            this.DeleteFlightCommand = new RelayCommand(DeleteFlight);
            this.ClearCommand = new RelayCommand(Clear);
            this.SelectGlobalFlightCommand = new RelayCommand(SelectGlobalFlight);
            this.RefreshFlightsCommand = new RelayCommand(RefreshFlights);

            //Init
            this.MinTimePicker = new DigitalTime(TimeSpan.Zero);
            this.MaxTimePicker = new DigitalTime(TimeSpan.FromHours(24).Add(TimeSpan.FromSeconds(-1)));
            this.Flights = new ObservableCollection<Flight>();
            this.PanelFlight = new Flight() { StartTime = DateTime.Now, };
        }

        //Methods
        public async void FlightSettingsLoaded()
        {
            await this.SyncronizeFlights();
        }

        public async void UpdateFlight()
        {
            UpdateFlightRequestResult updateResult = await this.FlightsApi.UpdateFlight(this.PanelFlight);

            if (updateResult.HasError && this.InternetConnectionService.CheckInternetConnection() == false)
            {
                this.MessageBoxService.Show("Internet connection error...");
            }
            else if (updateResult.HasError)
            {
                this.MessageBoxService.Show("Something went wrong on update...");
            }

            await this.SyncronizeFlights();
        }

        public void Clear()
        {
            this.PanelFlight = new Flight() { StartTime = DateTime.Now, };
            this.RaisePropertyChanged("PanelFlight");
        }

        public async void DeleteFlight()
        {
            DeleteFlightRequestResult deleteResult = await this.FlightsApi.DeleteFlight(this.PanelFlight.Id);

            if (deleteResult.HasError && this.InternetConnectionService.CheckInternetConnection() == false)
            {
                this.MessageBoxService.Show("Internet connection error...");
            }
            else if (deleteResult.HasError)
            {
                this.MessageBoxService.Show("Something went wrong on deleting flight...");
            }
            else
            {
                this.PanelFlight = new Flight();
                this.RaisePropertyChanged("PanelFlight");
            }

            await this.SyncronizeFlights();
        }

        public async void CreateFlight()
        {
            CreateFlightRequestResult createResult = await this.FlightsApi.CreateFlight(this.PanelFlight);

            if (createResult.HasError && this.InternetConnectionService.CheckInternetConnection() == false)
            {
                this.MessageBoxService.Show("Internet connection error...");
            }
            else if (createResult.HasError)
            {
                this.MessageBoxService.Show("Something went wrong on creating flight...");
            }
            else
            {
                this.PanelFlight = new Flight();
                this.RaisePropertyChanged("PanelFlight");
            }

            await this.SyncronizeFlights();
        }

        public void SelectGlobalFlight()
        {
            if (this.SelectedFlight == null)
            {
                this.MessageBoxService.Show("Select any flight from list, or edit file manually...");
                return;
            }

            this.GlobalSelectedFlight = this.SelectedFlight;
            this.FlightManager.SelectFlight(this.SelectedFlight.Id);
            this.RaisePropertyChanged("GlobalSelectedFlight");
        }

        public void RefreshFlights()
        {
            SyncronizeFlights();
        }

        public async Task SyncronizeFlights()
        {
            await this.FlightManager.Synchronize();

            if (this.InternetConnectionService.CheckInternetConnection() == true)
            {
                GetFlightsRequestResult flightsResult = await this.FlightsApi.GetFlights();
                this.Flights.ClearF().AddRange(flightsResult.Flights);
                this.RaisePropertyChanged("Flights");

                if (this.FlightManager.SelectedFlight != null)
                {
                    foreach (Flight flightsFromList in this.Flights)
                    {
                        if (this.FlightManager.SelectedFlight.Id == flightsFromList.Id && 
                            !this.FlightManager.Equals(flightsFromList))
                        {
                            this.FlightManager.SelectFlight(flightsFromList.Id);
                            this.GlobalSelectedFlight = this.FlightManager.SelectedFlight;
                            this.PanelFlight = this.FlightManager.SelectedFlight.Clone();
                            this.RaisePropertyChanged("GlobalSelectedFlight");
                            this.RaisePropertyChanged("PanelFlight");
                        }
                    }
                }
            }
        }
    }
}
