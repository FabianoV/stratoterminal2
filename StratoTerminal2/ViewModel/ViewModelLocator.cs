using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using Microsoft.Practices.ServiceLocation;
using StratoTerminal2.Services.Contract;
using StratoTerminal2.Services.Contract.Api;
using StratoTerminal2.Services.Implementation;
using StratoTerminal2.Services.Implementation.Api;

namespace StratoTerminal2.ViewModel
{
    public class ViewModelLocator
    {
        public ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            // Services
            SimpleIoc.Default.Register<ICOMPortConfig, DataListenerService>();
            SimpleIoc.Default.Register<IAuthApi, AuthApi>();
            SimpleIoc.Default.Register<IFlightsApi, FlightsApi>();
            SimpleIoc.Default.Register<IFramesApi, FramesApi>();
            SimpleIoc.Default.Register<IBingMapApi, BingMapApi>();
            SimpleIoc.Default.Register<IFrameFactory, FrameFactory>();
            SimpleIoc.Default.Register<IMessageBoxService, MessageBoxService>();
            SimpleIoc.Default.Register<ILoggerCSV, LoggerCSV>();
            SimpleIoc.Default.Register<ILoggerRaw, LoggerRaw>();
            SimpleIoc.Default.Register<IFrameHolder, FrameHolder>();
            SimpleIoc.Default.Register<IFlightManager, FlightManager>();
            SimpleIoc.Default.Register<IFlightFactory, FlightFactory>();
            SimpleIoc.Default.Register<ICommandManagementService, CommandManagementService>();
            SimpleIoc.Default.Register<IInternetConectionService, InternetConnectionService>();

            // View Models
            SimpleIoc.Default.Register<ViewModel.Main>();
            SimpleIoc.Default.Register<ViewModel.Telemetry>();
            SimpleIoc.Default.Register<ViewModel.Map>();
            SimpleIoc.Default.Register<ViewModel.Terminal>();
            SimpleIoc.Default.Register<ViewModel.InputOutputPanel>();
            SimpleIoc.Default.Register<ViewModel.SettingsPanel>();
            SimpleIoc.Default.Register<ViewModel.FlightSettings>();
            SimpleIoc.Default.Register<ViewModel.FramesSettings>();
        }

        //ViewModels
        public ViewModel.Main Main { get { return ServiceLocator.Current.GetInstance<ViewModel.Main>(); } }
        public ViewModel.Telemetry Telemetry { get { return ServiceLocator.Current.GetInstance<ViewModel.Telemetry>(); } }
        public ViewModel.Map Map { get { return ServiceLocator.Current.GetInstance<ViewModel.Map>(); } }
        public ViewModel.Terminal Terminal { get { return ServiceLocator.Current.GetInstance<ViewModel.Terminal>(); } }
        public ViewModel.InputOutputPanel InputOutput { get { return ServiceLocator.Current.GetInstance<ViewModel.InputOutputPanel>(); } }
        public ViewModel.SettingsPanel SettingsPanel { get { return ServiceLocator.Current.GetInstance<ViewModel.SettingsPanel>(); } }
        public ViewModel.FlightSettings FlightSettings { get { return ServiceLocator.Current.GetInstance<ViewModel.FlightSettings>(); } }
        public ViewModel.FramesSettings FramesSettings { get { return ServiceLocator.Current.GetInstance<ViewModel.FramesSettings>(); } }


        //Services
        public Services.Contract.Api.IAuthApi AuthApi { get { return ServiceLocator.Current.GetInstance<Services.Contract.Api.IAuthApi>(); } }
        public Services.Contract.Api.IFlightsApi FlightsApi { get { return ServiceLocator.Current.GetInstance<Services.Contract.Api.IFlightsApi>(); } }
        public Services.Contract.Api.IFramesApi FramesApi { get { return ServiceLocator.Current.GetInstance<Services.Contract.Api.IFramesApi>(); } }
        public Services.Contract.Api.IBingMapApi BingMapApi { get { return ServiceLocator.Current.GetInstance<Services.Contract.Api.IBingMapApi>(); } }
        public Services.Contract.ICOMPortConfig DataListenerService { get { return ServiceLocator.Current.GetInstance<Services.Contract.ICOMPortConfig>(); } }
        public Services.Contract.IFrameFactory FrameFactoryService { get { return ServiceLocator.Current.GetInstance<Services.Contract.IFrameFactory>(); } }
        public Services.Contract.ILoggerCSV LoggerCSVService { get { return ServiceLocator.Current.GetInstance<Services.Contract.ILoggerCSV>(); } }
        public Services.Contract.ILoggerRaw LoggerRaw { get { return ServiceLocator.Current.GetInstance<Services.Contract.ILoggerRaw>(); } }
        public Services.Contract.IMessageBoxService MessageBoxService { get { return ServiceLocator.Current.GetInstance<Services.Contract.IMessageBoxService>(); } }
        public Services.Contract.IFrameHolder FrameHolderService { get { return ServiceLocator.Current.GetInstance<Services.Contract.IFrameHolder>(); } }
        public Services.Contract.IFlightManager FlightManager { get { return ServiceLocator.Current.GetInstance<Services.Contract.IFlightManager>(); } }
        public Services.Contract.IFlightFactory FlightFactory { get { return ServiceLocator.Current.GetInstance<Services.Contract.IFlightFactory>(); } }
        public Services.Contract.ICommandManagementService CommandManagementService { get { return ServiceLocator.Current.GetInstance<Services.Contract.ICommandManagementService>(); } }
        public Services.Contract.IInternetConectionService InternetConectionService { get { return ServiceLocator.Current.GetInstance<Services.Contract.IInternetConectionService>(); } }

        public static void Cleanup()
        {
            // TODO Clear the ViewModels
        }
    }
}