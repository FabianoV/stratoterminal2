﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using StratoTerminal2.Models;
using StratoTerminal2.Models.AzureModels;
using StratoTerminal2.Models.RequestsResults;
using StratoTerminal2.Services.Contract;
using StratoTerminal2.Services.Contract.Api;
using StratoTerminal2.Tools;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StratoTerminal2.ViewModel
{
    public class FramesSettings : ViewModelBase
    {
        //Services
        public IFramesApi FramesApi { get; set; }
        public IFlightManager FlightManager { get; set; }
        public IMessageBoxService MessageBoxService { get; set; }
        public IInternetConectionService InternetConnetionService { get; set; }

        //Properties
        public ObservableCollection<FrameAzure> Frames { get; set; }

        //Commands
        public RelayCommand FramesSettingsLoadedCommand { get; set; }

        //Constructor
        public FramesSettings(IFramesApi framesApi, IFlightManager flightManager, IMessageBoxService messageBoxService,
            IInternetConectionService internetConnectionService)
        {
            //Services
            this.FramesApi = framesApi;
            this.FlightManager = flightManager;
            this.MessageBoxService = messageBoxService;
            this.InternetConnetionService = internetConnectionService;

            //Commands
            this.FramesSettingsLoadedCommand = new RelayCommand(this.FramesSettingsLoaded);

            //Initialize
            this.Frames = new ObservableCollection<FrameAzure>();
        }

        //Methods
        public async void FramesSettingsLoaded()
        {
            this.DownloadFrames();
        }

        public async void Refresh()
        {
            this.DownloadFrames();
        }

        public async void DownloadFrames()
        {
            Flight currentGlobalFlight = this.FlightManager.SelectedFlight;
            if (currentGlobalFlight != null && this.InternetConnetionService.CheckInternetConnection() == true)
            {
                GetFramesFromFlightRequestResult result = await this.FramesApi.GetFrames(currentGlobalFlight.Id);

                if (!result.HasError)
                {
                    this.Frames.ClearF().AddRange(result.Frames);
                    this.RaisePropertyChanged("Frames");
                }
            }
        }
    }
}
