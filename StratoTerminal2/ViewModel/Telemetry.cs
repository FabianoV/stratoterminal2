﻿using GalaSoft.MvvmLight;
using StratoTerminal2.Models;
using StratoTerminal2.Services.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace StratoTerminal2.ViewModel
{
    public class Telemetry : ViewModelBase
    {
        //Services
        IFrameFactory frameFactory { get; set; }
        IFrameHolder frameHolder { get; set; }

        //Binding Properties
        public Frame LastFrame { get; set; }

        //Constructor
        public Telemetry(IFrameHolder frameHolder, IFrameFactory frameFactory)
        {
            //Services
            this.frameHolder = frameHolder;
            this.frameFactory = frameFactory;

            //Init
            this.frameFactory.FrameCreated += FrameFactory_FrameCreated;
        }

        //Methods
        private void FrameFactory_FrameCreated(object sender, Models.EventArgs.FrameCreatedEventArgs e)
        {
            this.LastFrame = e.NewFrame;
            this.RaisePropertyChanged("LastFrame");
        }
    }
}
