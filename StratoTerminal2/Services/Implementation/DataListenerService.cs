﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
using StratoTerminal2.Models;
using StratoTerminal2.Services.Contract;
using StratoTerminal2.Models.Enums;
using StratoTerminal2.Models.RequestsResults;
using StratoTerminal2.Services.Contract.Api;

namespace StratoTerminal2.Services.Implementation
{
    public class DataListenerService : ICOMPortConfig
    {
        //Services
        public IFramesApi FramesApi { get; set; }
        public IFrameFactory FrameFactory { get; set; }
        public IMessageBoxService MessageBoxService { get; set; }
        public ILoggerCSV LoggerCSV { get; set; }
        public ILoggerRaw LoggerRaw { get; set; }
        public IFlightManager FlightManager { get; set; }
        public IInternetConectionService InternetConnectionService { get; set; }

        //Constants
        private const char LINE_FEED_CHAR = (char)10;
        private const char CARRIAGE_RETURN_CHAR = (char)13;

        //Properties
        public string Bufor { get; set; }
        public SerialPort Port { get; set; }
        public bool IsPortOpen { get { return this.Port != null ? this.Port.IsOpen : false; } }
        public List<Frame> Frames { get; set; }

        //Binding Properties
        public Frame LastFrame { get { return this.Frames.LastOrDefault(); } }

        //Constructor
        public DataListenerService(IFramesApi framesApi, IFrameFactory frameFactory, IMessageBoxService messageBoxService, 
            ILoggerCSV loggerCsv, ILoggerRaw loggerRaw, IFlightManager flightManager, IInternetConectionService internetconnectionService)
        {
            //Services
            this.FramesApi = framesApi;
            this.FrameFactory = frameFactory;
            this.MessageBoxService = messageBoxService;
            this.LoggerCSV = loggerCsv;
            this.LoggerRaw = loggerRaw;
            this.FlightManager = flightManager;
            this.InternetConnectionService = internetconnectionService;

            //Init
            this.Port = null;
            this.Frames = new List<Frame>();
        }

        //Methods
        public void CreatePort(string portName, int baudRate)
        {
            this.Port = new SerialPort(portName, baudRate, Parity.None, 8, StopBits.One);
            this.Port.DataReceived += Port_DataReceived;
            this.Port.Open();
        }

        public void ClosePort()
        {
            if (this.Port.IsOpen)
            {
                this.Port.Close();
            }
        }

        /// <summary>
        /// Metoda służąca do odczytu ramek z portu COM.
        /// Nie zawsze przyvhodzi cała ramka. Wtedy sprawdzana jest czy nie ma znaku końca linii, a jeśli ma to odczytaj 
        /// tekst od początku do końca linii i potraktuj go jako informację o gotowej ramce.
        /// </summary>
        private async void Port_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            //Powtarzaj odczyt ramki dopóki znajdują się tutaj znaki /r lub /n, 
            //czasami mogą przyjść 2 ramki narazi i trzeba powtórzyć odczyt.
            //Patrz warunek na końcu.
            do
            {
                this.Bufor += this.Port.ReadExisting(); //Odczytaj dane z portu COM do bufora
                string currentFrameRaw = null;

                if (this.Bufor.Contains(CARRIAGE_RETURN_CHAR)) //Odczytaj ramkę z danych jeśli istnieje w nich pełna ramka ze znakiem /r
                {
                    currentFrameRaw = this.Bufor.Substring(0, this.Bufor.IndexOf(CARRIAGE_RETURN_CHAR) + 1); //Wytnij obecną ramkę z bufora
                    this.Bufor = this.Bufor.Remove(0, currentFrameRaw.Length); //Usuń wyciętą ramkę z bufora
                }
                else
                {
                    return; // nie ma żadnej ramki więc zakończ działanie odczytu.
                }

                //Stwórz nowy obiekt ramka z odczytanych danych i zmapuj wszystkie informacje
                Frame currentFrame = null;
                if (!string.IsNullOrEmpty(currentFrameRaw))
                {
                    currentFrame = this.FrameFactory.Create(currentFrameRaw);
                    this.Frames.Add(currentFrame); //Dodaj obecną ramkę do zbioru wszystkich ramek

                    //Logger.LogInfo(string.Format("Odebrano i stworzono ramkę: {0}, Status ramki: {1}", this.obecnaRamkaString, obecnaRamka.TypRamki));
                    //if (currentFrame.TypRamki == Frame.Typ.Dane || currentFrame.TypRamki == Ramka.Typ.Uszkodzona)
                    //{
                    //    //TODO
                    //    //Logger.LogInfo("ReadData -> Obecna ramka: " + obecnaRamka.ToString());
                    //}
                }
                else
                {
                    //Logger.LogInfo("Brak kompletnej ramki: " + this.obecnaRamkaString);
                    return;
                }

                //Oznacz lot ramki
                currentFrame.FlightId = this.FlightManager.SelectedFlight.Id;

                //Loguj
                this.LoggerRaw.LogFrame(currentFrame);
                this.LoggerCSV.LogFrame(currentFrame);

                //Wyślij ramkę na serwer
                if (InternetConnectionService.CheckInternetConnection() == true)
                {
                    CreateFrameRequestResult result = await this.FramesApi.CreateFrame(currentFrame);
                    if (result.HasError)
                    {
                        // if error or no internet connetion then try sae frames
                    }
                    // next try send frames than can not be send before.
                }

            } while (this.Bufor.Contains(CARRIAGE_RETURN_CHAR) || this.Bufor.Contains(LINE_FEED_CHAR));
        }

        public List<string> GetComPortsNames()
        {
            return SerialPort.GetPortNames().ToList();
        }
    }
}
