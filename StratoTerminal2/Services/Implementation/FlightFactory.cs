﻿using StratoTerminal2.Services.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StratoTerminal2.Models;
using StratoTerminal2.Models.RequestsResults;
using StratoTerminal2.Services.Contract.Api;

namespace StratoTerminal2.Services.Implementation
{
    public class FlightFactory : IFlightFactory
    {
        //Services
        public IFlightsApi FlightsApi { get; set; }

        //Constructor
        public FlightFactory(IFlightsApi flightsApi)
        {
            //Services
            this.FlightsApi = flightsApi;

            //TODO: Inject logger 
        }

        //Methods
        public async Task<Flight> CreateFlight(string name, string deviceName, string STInscanceName, string description = "", bool test = false)
        {
            return await this.CreateFlight(name, DateTime.Now, deviceName, STInscanceName, description, test);
        }

        public async Task<Flight> CreateFlight(string name, DateTime start, string deviceName, string STInscanceName, string description, bool test)
        {
            Flight flight = new Flight()
            {
                Name = name,
                StartTime = start,
                DeviceName = deviceName,
                STInstanceName = STInscanceName,
                Description = description,
                IsTest = Convert.ToInt32(test),
            };

            //CreateFlightRequestResult result = await this.FlightsApi.CreateFlight(flight);

            ////TODO: Log success or fail
            //if (result.IsFlightCreatedSuccessfully)
            //{
            //    // log success
            //}
            //else
            //{
            //    // log fail
            //}

            //return result.Flight;

            return flight;
        }
    }
}
