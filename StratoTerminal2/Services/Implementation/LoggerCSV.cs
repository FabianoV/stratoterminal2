﻿using StratoTerminal2.Models;
using StratoTerminal2.Services.Contract;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StratoTerminal2.Services.Implementation
{
    public class LoggerCSV : ILoggerCSV
    {
        //Fields
        string path;
        bool isConfigured;

        //Properties
        public string Path { get { return path; } set { path = value; } }
        public string Filename { get { return System.IO.Path.GetFileName(path); } }
        public bool IsConfigured { get { return this.isConfigured; } }

        // Constructor
        public LoggerCSV()
        {
            Configure();
        }

        //Methods
        public void LogFrame(Frame frame)
        {
            using (StreamWriter handler = File.AppendText(this.Path))
            {
                handler.WriteLine(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10}" , frame.Id, 
                    frame.State, frame.GPSTime, frame.InnerTemperature, frame.OuterTemperature, frame.Battery, 
                    frame.Fix, frame.SatelitesCount,  frame.Height, frame.Lattitude, frame.Longtitude, 
                    frame.HorizontalSpeed, frame.VerticalSpeed, frame.IOControl, frame.Ascent));
            }
        }
        public void LogLine(string line)
        {
            using (StreamWriter handler = File.AppendText(this.Path))
            {
                handler.WriteLine(line);
            }
        }
        // Default configuration method
        public void Configure()
        {
            string directory = "csv";
            this.Path = directory + "/" + DateTime.Now.ToString("yyyyMMdd hhmm") + ".csv";
            this.Path = System.IO.Path.GetFullPath(Path);

            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }

            using (StreamWriter sw = File.CreateText(this.Path))
            {
                sw.WriteLine("Nr Ramki;Typ Ramki;Czas UTC;Temp Wewn.;Temp Zewn.;Bateria;Fix;Satelity;" +
                    "Wysokosc;Pozycja Dl;Pozycja Sz.;Predkosc H;Predkosc W;io_Control;Wznoszenie obl.");
            }
            this.isConfigured = true;
        }
        public void Configure(string filename)
        {
            this.Path = System.IO.Path.GetFullPath(filename);
            string dirPath = System.IO.Path.GetDirectoryName(this.Path);
            if (!string.IsNullOrWhiteSpace(dirPath) && !Directory.Exists(dirPath))
            {
                Directory.CreateDirectory(dirPath);
            }

            using (StreamWriter sw = File.CreateText(this.Path))
            {
                sw.WriteLine("Nr Ramki;Typ Ramki;Czas UTC;Temp Wewn.;Temp Zewn.;Bateria;Fix;Satelity;" +
                    "Wysokosc;Pozycja Dl;Pozycja Sz.;Predkosc H;Predkosc W;io_Control;Wznoszenie obl.");
            }
            this.isConfigured = true;
        }
    }
}
