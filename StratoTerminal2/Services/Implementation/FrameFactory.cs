﻿using StratoTerminal2.Services.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StratoTerminal2.Models;
using StratoTerminal2.Models.Enums;
using StratoTerminal2.Tools;
using StratoTerminal2.Models.EventArgs;

namespace StratoTerminal2.Services.Implementation
{
    public class FrameFactory : IFrameFactory
    {
        //Services
        public IFrameHolder FrameHolderService { get; set; }

        //Constants
        private const char LINE_FEED_CHAR = (char)10;
        private const char CARRIAGE_RETURN_CHAR = (char)13;
        private const char FRAME_SEPARATOR = ';';

        //Events
        public event EventHandler<FrameCreatedEventArgs> FrameCreated;

        //Properties
        public List<Frame> Frames { get { return this.FrameHolderService.Frames; } }
        public Frame LastFrame { get { return Frames.LastOrDefault(); } }

        //Constructor
        public FrameFactory(IFrameHolder frameHolderService)
        {
            //Services
            this.FrameHolderService = frameHolderService;
        }

        //Methods
        public Frame Create(string rawFrame)
        {
            Frame frame = new Frame();
            frame.GPSTime = DateTime.Now;
            frame.Raw = rawFrame;

            //Set default values to frame
            frame.GPSTime = DateTime.MinValue;
            frame.InnerTemperature = double.NaN;
            frame.OuterTemperature = double.NaN;
            frame.Battery = double.NaN;
            frame.Fix = 0;
            frame.SatelitesCount = 0;
            frame.Height = double.NaN;
            frame.Lattitude = double.NaN;
            frame.Longtitude = double.NaN;
            frame.HorizontalSpeed = double.NaN;
            frame.VerticalSpeed = double.NaN;
            frame.IOControl = string.Empty;
            frame.DataPayload = string.Empty;

            //TODO: Log raw Frame at creation
            if (!rawFrame.EndsWith(LINE_FEED_CHAR.ToString()) && !rawFrame.EndsWith(CARRIAGE_RETURN_CHAR.ToString()))
            {
                string error = string.Format("Niepoprawna ramka. Ostatni znak: \"{0}\". Numer ASCII: {1}", rawFrame.Last(), (int)rawFrame.Last());
                // Logger.LogError(error);
                throw new Exception(error);
            }

            //Mapowanie tekstu ramki na obiekt Ramka
            frame.Data = rawFrame.Split(FRAME_SEPARATOR);

            if (rawFrame.StartsWith(">> RADIO OFF <<"))
            {
                frame.State = FrameState.RadioOff;
            }
            else if (rawFrame.StartsWith(">> RADIO ON <<"))
            {
                frame.State = FrameState.RadioOn;
            }
            else if (rawFrame.StartsWith(">> INIT OK <<"))
            {
                frame.State = FrameState.InitOk;
            }
            else if (rawFrame.StartsWith("V"))
            {
                frame.State = FrameState.CodeVersion;
            }
            else if (rawFrame.Contains("ERROR"))
            {
                frame.State = FrameState.Error;
            }
            else if (frame.Data.Length < 12)
            {
                frame.State = FrameState.Damaged;
            }
            else
            {
                if (frame.Raw.Contains("|LAST_OK|"))
                {
                    frame.State = FrameState.DataOnDemand;
                }
                else
                {
                    frame.State = FrameState.Data;
                }

                frame.Id = frame.Data[0];

                try
                {
                    frame.GPSTime = frame.Data[1].ToDateTimeUTC();
                    frame.InnerTemperature = frame.Data[2].ToDouble();
                    frame.OuterTemperature = frame.Data[3].ToDouble();
                    frame.Battery = frame.Data[4].ToDouble();
                    frame.Fix = frame.Data[5].ToInt();
                    frame.SatelitesCount = frame.Data[6].ToInt();
                    frame.Height = frame.Data[7].ToDouble();
                    frame.Lattitude = frame.Data[8].ToDouble();
                    frame.Longtitude = frame.Data[9].ToDouble();
                    frame.HorizontalSpeed = frame.Data[10].ToDouble();
                    frame.Ascent = frame.Data[11].ToDouble();
                    frame.IOControl = frame.Data[12];
                    frame.DataPayload = frame.Data[13];
                }
                catch (Exception)
                {
                    //Jeśli nastąpił błąd konwersji to zaznacz jako ukodzoną, nie dotyczy Id
                    frame.State = FrameState.Damaged;
                }

                //Znajdź poprzenią ramkę i oblicz na podstawie jej wysokości prędkość obecnej ramki.
                Frame lastFrameWithHeight = this.FrameHolderService.GetLastFrameByHeight(frame.Id, frame.Height, frame.GPSTime);
                if (lastFrameWithHeight != null && frame.State != FrameState.Damaged)
                {
                    this.CalculateBothVelocity(frame, lastFrameWithHeight);
                }
                else
                {
                    frame.VerticalSpeed = double.NaN;
                    frame.HorizontalSpeed = double.NaN;
                }
            }

            //Fire event frame created
            this.FrameCreated?.Invoke(this, new FrameCreatedEventArgs()
            {
                LastFrame = this.FrameHolderService.LastFrame,
                NewFrame = frame,
            });

            this.FrameHolderService.AddFrame(frame);
            return frame;
        }

        //temperatura: wartości -70 do 50 'C - POPRAWNE; wartości poniżej -80 brak czujnika; brak wartości lub nie-liczba lub sieczka - ERROR
        //napięcie: 0.000 - brak pomiaru; między 2.000-11.000 POPRAWNE; wartości ujemne albo inne niż liczba NIEPOPRAWNE
        //fix: 1,2,3 - POPRAWNE; brak: jeśli w sekccji danych jest |LAST_OK| - POPRAWNE; brak: jeśli brakuje też czasu utc - POPRAWNE; brak lub losowe wartości inne niż liczba - NIEPOPRAWNE
        //satelity: wartości 00-12 - POPRAWNE; wartości ?? - POPRAWNE[brak fix]; każde inne - NIEPOPRAWNE
        //wysokość: wartości od -300 do 50'000 - POPRAWNE; wartość ?? - POPRAWNE [brak fix]; każde inne - NIEPOPRAWNE
        //pozycja: wartości liczbowe - POPRAWNE; 
        //szerokość: od -90.00000 do 90.00000 - poprawne
        //długość: od -180.00000 do 180.00000 - poprawne
        //każde inne - NIEPOPRAWNE
        //prędkość pozioma: waerości 0.000-150.000 - POPRAWNE; wartości ujemne albo nie-liczba - NIEPOPRAWNE
        //wznoszenie(przybliżone, liczone w gondoli +-1m/s): wartości od -100 do 10 [m/s] - POPRAWNE; każde inne lub nie liczba - NIEPOPRAWNE
        //IO: wartości string 0 i 1 - poprawne; zawsze parzyście i parami(WE, WY)
        //pole danych: do 85B maksymalnie; zawsze znak > na początku; dowolne dane string lub binarne; puste: POPRAWNE(brak danych); Zawiera # lub CR - zabronione; dozwolony separator ':'   ; zawiera na końcu |LAST_OK| - dozwolone (ramka na żądanie)

        /// <summary>
        /// Obliczanie prędkości:
        /// Pierwsza prędkość to prędkość pozioma.
        /// Druga prędkość to prędkość pionowa - opadania lub wznoszenia.
        /// </summary>
        public void CalculateBothVelocity(Frame currentFrame, Frame lastFrame)
        {
            try
            {
                if (currentFrame.Fix == 2 || currentFrame.Fix == 3)
                {
                    double timeDelta = (currentFrame.GPSTime - lastFrame.GPSTime).TotalSeconds;
                    currentFrame.VerticalSpeed = (currentFrame.Height - lastFrame.Height) / timeDelta;
                    //currentFrame.HorizontalSpeed = currentFrame.HorizontalSpeed * 1.852; // przelicza z węzłów na km/h

                    //TODO: Logger.LogData("Delta czas: " + timeDelta);
                }
                else
                {
                    currentFrame.VerticalSpeed = double.NaN;
                }
            }
            catch (Exception ex)
            {
                //TODO: MessageBox.Show("Błąd w metodzie CalculateBothVelocity. Treść błędu: " + Environment.NewLine + ex);
            }
        }
    }
}
