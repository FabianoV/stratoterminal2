﻿using Newtonsoft.Json;
using StratoTerminal2.Models;
using StratoTerminal2.Models.RequestsResults;
using StratoTerminal2.Services.Contract.Api;
using StratoTerminal2.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace StratoTerminal2.Services.Implementation.Api
{
    public class FramesApi : ApiBase, IFramesApi
    {
        public async Task<GetFramesFromFlightRequestResult> GetFrames(int flightId)
        {
            //Initialize variables
            base.BeforeRequest();
            string RequestUri = BASE_ADDRESS + "api/Frame/ByFlight/" + flightId;
            GetFramesFromFlightRequestResult result = new GetFramesFromFlightRequestResult();
            MeasuerTimeLogHelper token = new MeasuerTimeLogHelper(RequestUri);

            //Check internet connection
            if (base.InternetConectionService.CheckInternetConnection() == false)
            {
                result.SetNoInternetConnectionError();
                return result;
            }

            //Make request
            try
            {
                using (HttpClient httpClient = new HttpClient())
                {
                    this.PrepareHeader(httpClient, contentType: "application/json", noCache: true);
                    using (HttpResponseMessage response = await httpClient.GetAsync(RequestUri))
                    {
                        string responseText = await response.Content.ReadAsStringAsync();
                        result.MapResult(responseText, response.StatusCode, null, null);
                        token.FlagRequestDownloaded(result.ResponseMessage);
                    }
                }
            }
            catch (WebException ex)
            {
                result.HandleError(ex);
            }

            //Finalize
            token.EndMeasure();
            base.AfterRequest();

            return result;
        }

        public async Task<GetFrameRequestResult> GetFrame(int frameId)
        {
            throw new NotImplementedException();
        }

        public async Task<GetFrameRequestResult> GetFrame(string frameId, int flightId)
        {
            throw new NotImplementedException();
        }

        public async Task<CreateFrameRequestResult> CreateFrame(Frame frame)
        {
            //Initialize variables
            base.BeforeRequest();
            string RequestUri = BASE_ADDRESS + "api/Frame";
            CreateFrameRequestResult result = new CreateFrameRequestResult();
            string jsonParams = JsonConvert.SerializeObject(frame);
            MeasuerTimeLogHelper token = new MeasuerTimeLogHelper(RequestUri, jsonParams);

            //Check internet connection
            if (base.InternetConectionService.CheckInternetConnection() == false)
            {
                result.SetNoInternetConnectionError();
                return result;
            }

            //Make request
            try
            {
                using (HttpClient httpClient = new HttpClient())
                {
                    this.PrepareHeader(httpClient, contentType: "application/json", noCache: true);
                    using (HttpResponseMessage response = await httpClient.PostAsync(RequestUri,
                        new StringContent(jsonParams, Encoding.UTF8, "application/json")))
                    {
                        string responseText = await response.Content.ReadAsStringAsync();
                        result.MapResult(responseText, response.StatusCode, null, null);
                        token.FlagRequestDownloaded(result.ResponseMessage);
                    }
                }
            }
            catch (WebException ex)
            {
                result.HandleError(ex);
            }
            catch (Exception ex)
            {
                result.HandleError(ex);
            }

            //Finalize
            token.EndMeasure();
            base.AfterRequest();

            return result;
        }

        public async Task<UpdateFrameRequestResult> UpdateFrame(Frame frame)
        {
            throw new NotImplementedException();
        }

        public async Task<DeleteFrameRequestResult> DeleteFrame(int frameId)
        {
            throw new NotImplementedException();
        }

        public async Task<DeleteFrameRequestResult> DeleteFrame(string frameId, int flightId)
        {
            throw new NotImplementedException();
        }
    }
}
