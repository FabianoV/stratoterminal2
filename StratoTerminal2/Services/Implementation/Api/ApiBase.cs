﻿using GalaSoft.MvvmLight.Ioc;
using StratoTerminal2.Services.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace StratoTerminal2.Services.Implementation.Api
{
    public abstract class ApiBase
    {
        //Consts
        public const string BASE_ADDRESS = "http://stratoterminalapi.azurewebsites.net/";
        //public const string BASE_ADDRESS = "http://localhost:59562/";
        public const string BASE_ADDRESS_TESTS = "http://localhost:59562/api/";

        //Services
        public IInternetConectionService InternetConectionService { get; set; }

        //Properties
        public bool IsTests { get; set; }
        public string ApiAddress { get { return IsTests ? BASE_ADDRESS_TESTS : BASE_ADDRESS; } }

        //Constructor
        public ApiBase()
        {
            this.InternetConectionService = SimpleIoc.Default.GetInstance<IInternetConectionService>();
        }

        //Methods
        protected async void PrepareHeader(HttpClient httpClient, string contentType, bool noCache)
        {
            httpClient.DefaultRequestHeaders.Add("os", "windows");
            if (noCache) httpClient.DefaultRequestHeaders.Add("Cache-Control", "no-cache");
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(contentType));
        }

        protected void BeforeRequest()
        {

        }
        protected void AfterRequest()
        {

        }
    }
}
