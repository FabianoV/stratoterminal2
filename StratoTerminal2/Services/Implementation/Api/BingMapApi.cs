﻿using StratoTerminal2.Models;
using StratoTerminal2.Models.RequestsResults;
using StratoTerminal2.Services.Contract.Api;
using StratoTerminal2.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace StratoTerminal2.Services.Implementation.Api
{
    public class BingMapApi : ApiBase, IBingMapApi
    {
        //Consts
        public const string BING_API_KEY = "AqwKR_azh6Duyf3rspur1R1CRFDe9dGEyNiutDqqCA91iRfWgv1C9CAKPsBt1Egx";

        public async Task<GetElevationRequestResult> GetElevation(Location location)
        {
            //Docs: How to read elevation from bingmaps api
            //http://www.learningbingmaps.com/tutorials/2013/11/23/obtain-altitude-above-sea-level-of-a-point-with-bing-maps-api
            //Initialize variables
            base.BeforeRequest();
            string RequestUri = "http://dev.virtualearth.net/REST/v1/Elevation/List?pts={lat},{lon}&key={api_key}"
                .Replace("{lat}", location.Lattitude.CommaToDot())
                .Replace("{lon}", location.Longtitude.CommaToDot())
                .Replace("{api_key}", BING_API_KEY);
            GetElevationRequestResult result = new GetElevationRequestResult();
            MeasuerTimeLogHelper token = new MeasuerTimeLogHelper(RequestUri);

            //Check internet connection
            if (base.InternetConectionService.CheckInternetConnection() == false)
            {
                result.SetNoInternetConnectionError();
                return result;
            }

            //Make request
            try
            {
                using (HttpClient httpClient = new HttpClient())
                {
                    this.PrepareHeader(httpClient, contentType: "application/json", noCache: true);
                    using (HttpResponseMessage response = await httpClient.GetAsync(RequestUri))
                    {
                        string responseText = await response.Content.ReadAsStringAsync();
                        result.MapResult(responseText, response.StatusCode, null, null);
                        token.FlagRequestDownloaded(result.ResponseMessage);
                    }
                }
            }
            catch (WebException ex)
            {
                result.HandleError(ex);
            }

            //Finalize
            token.EndMeasure();
            base.AfterRequest();

            return result;
        }
    }
}
