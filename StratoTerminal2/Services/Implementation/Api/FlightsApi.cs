﻿using Newtonsoft.Json;
using StratoTerminal2.Models;
using StratoTerminal2.Models.RequestsResults;
using StratoTerminal2.Services.Contract.Api;
using StratoTerminal2.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace StratoTerminal2.Services.Implementation.Api
{
    public class FlightsApi : ApiBase, IFlightsApi
    {
        public async Task<CreateFlightRequestResult> CreateFlight(Flight flight)
        {
            //Initialize variables
            base.BeforeRequest();
            string RequestUri = BASE_ADDRESS + "api/Flight";
            CreateFlightRequestResult result = new CreateFlightRequestResult();
            string jsonParams = JsonConvert.SerializeObject(flight);
            MeasuerTimeLogHelper token = new MeasuerTimeLogHelper(RequestUri, jsonParams);

            //Check internet connection
            if (base.InternetConectionService.CheckInternetConnection() == false)
            {
                result.SetNoInternetConnectionError();
                return result;
            }

            //Make request
            try
            {
                using (HttpClient httpClient = new HttpClient())
                {
                    this.PrepareHeader(httpClient, contentType: "application/json", noCache: true);
                    using (HttpResponseMessage response = await httpClient.PostAsync(RequestUri,
                        new StringContent(jsonParams, Encoding.UTF8, "application/json")))
                    {
                        string responseText = await response.Content.ReadAsStringAsync();
                        result.MapResult(responseText, response.StatusCode, null, null);
                        token.FlagRequestDownloaded(result.ResponseMessage);
                    }
                }
            }
            catch (WebException ex)
            {
                result.HandleError(ex);
            }
            catch (Exception ex)
            {
                result.HandleError(ex);
            }

            //Finalize
            token.EndMeasure();
            base.AfterRequest();

            return result;
        }

        public async Task<GetFlightsRequestResult> GetFlights()
        {
            //Initialize variables
            base.BeforeRequest();
            string RequestUri = BASE_ADDRESS + "api/Flight";
            GetFlightsRequestResult result = new GetFlightsRequestResult();
            MeasuerTimeLogHelper token = new MeasuerTimeLogHelper(RequestUri);

            //Check internet connection
            if (base.InternetConectionService.CheckInternetConnection() == false)
            {
                result.SetNoInternetConnectionError();
                return result;
            }

            //Make request
            try
            {
                using (HttpClient httpClient = new HttpClient())
                {
                    this.PrepareHeader(httpClient, contentType: "application/json", noCache: true);
                    using (HttpResponseMessage response = await httpClient.GetAsync(RequestUri))
                    {
                        string responseText = await response.Content.ReadAsStringAsync();
                        result.MapResult(responseText, response.StatusCode, null, null);
                        token.FlagRequestDownloaded(result.ResponseMessage);
                    }
                }
            }
            catch (WebException ex)
            {
                result.HandleError(ex);
            }

            //Finalize
            token.EndMeasure();
            base.AfterRequest();

            return result;
        }

        public async Task<GetFlighRequestResult> GetFlight(int flightId)
        {
            //Initialize variables
            base.BeforeRequest();
            string RequestUri = BASE_ADDRESS + "api/Flight/" + flightId;
            GetFlighRequestResult result = new GetFlighRequestResult();
            MeasuerTimeLogHelper token = new MeasuerTimeLogHelper(RequestUri);

            //Check internet connection
            if (base.InternetConectionService.CheckInternetConnection() == false)
            {
                result.SetNoInternetConnectionError();
                return result;
            }

            //Make request
            try
            {
                using (HttpClient httpClient = new HttpClient())
                {
                    this.PrepareHeader(httpClient, contentType: "application/json", noCache: true);
                    using (HttpResponseMessage response = await httpClient.GetAsync(RequestUri))
                    {
                        string responseText = await response.Content.ReadAsStringAsync();
                        result.MapResult(responseText, response.StatusCode, null, null);
                        token.FlagRequestDownloaded(result.ResponseMessage);
                    }
                }
            }
            catch (WebException ex)
            {
                result.HandleError(ex);
            }
            catch (Exception ex)
            {
                result.HandleError(ex);
            }

            //Finalize
            token.EndMeasure();
            base.AfterRequest();

            return result;
        }

        public async Task<UpdateFlightRequestResult> UpdateFlight(Flight flight)
        {
            throw new NotImplementedException();
        }

        public async Task<DeleteFlightRequestResult> DeleteFlight(int flightId)
        {
            throw new NotImplementedException();
        }
    }
}
