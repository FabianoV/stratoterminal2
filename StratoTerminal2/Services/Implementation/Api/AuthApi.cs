﻿using StratoTerminal2.Models;
using StratoTerminal2.Models.RequestsResults;
using StratoTerminal2.Services.Contract.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StratoTerminal2.Services.Implementation.Api
{
    public class AuthApi : ApiBase, IAuthApi
    {
        public async Task<LoginRequestResult> Login(string login, string password)
        {
            throw new NotImplementedException();
        }

        public async Task<RegisterRequestResult> Register(string login, string password)
        {
            throw new NotImplementedException();
        }

        public async Task<UpdateUserRequestResult> UpdateUser(User user)
        {
            throw new NotImplementedException();
        }
    }
}
