﻿using StratoTerminal2.Services.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;

namespace StratoTerminal2.Services.Implementation
{
    public class InternetConnectionService : IInternetConectionService
    {
        public bool CheckInternetConnection()
        {
            try
            {
                Ping myPing = new Ping();
                String host = "google.com";
                byte[] buffer = new byte[32];
                int timeout = 1000;
                PingOptions pingOptions = new PingOptions();
                PingReply reply = myPing.Send(host, timeout, buffer, pingOptions);
                return (reply.Status == IPStatus.Success);
            }
            catch (Exception)
            {
                return false;
            }

            //// only recognizes changes related to Internet adapters
            //if (System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
            //{
            //    // however, this will include all adapters -- filter by opstatus and activity
            //    NetworkInterface[] interfaces = System.Net.NetworkInformation.NetworkInterface.GetAllNetworkInterfaces();
            //    return (from face in interfaces
            //            where face.OperationalStatus == OperationalStatus.Up
            //            where (face.NetworkInterfaceType != NetworkInterfaceType.Tunnel) && (face.NetworkInterfaceType != NetworkInterfaceType.Loopback)
            //            select face.GetIPv4Statistics()).Any(statistics => (statistics.BytesReceived > 0) && (statistics.BytesSent > 0));
            //}

            //return false;
        }
    }
}
