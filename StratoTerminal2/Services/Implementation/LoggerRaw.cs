﻿using StratoTerminal2.Services.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StratoTerminal2.Models;
using System.IO;

namespace StratoTerminal2.Services.Implementation
{
    public class LoggerRaw : ILoggerRaw
    {
        //Fields
        string path;
        bool isConfigured;

        //Properties
        public string Path { get { return path; } set { path = value; } }
        public string Filename { get { return System.IO.Path.GetFileName(path); } }
        public bool IsConfigured { get { return this.isConfigured; } }

        //Constructor
        public LoggerRaw()
        {
            this.Configure();
        }

        //Methods
        public void Configure()
        {
            string directory = "raw";
            this.Path = directory + "/" + DateTime.Now.ToString("raw yyyyMMdd hhmm") + ".log";
            this.Path = System.IO.Path.GetFullPath(Path);

            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }

            this.isConfigured = true;
        }

        public void Configure(string path)
        {
            this.Path = System.IO.Path.GetFullPath(path);
            string dirPath = System.IO.Path.GetDirectoryName(this.Path);
            if (!string.IsNullOrWhiteSpace(dirPath) && !Directory.Exists(dirPath))
            {
                Directory.CreateDirectory(dirPath);
            }
            this.isConfigured = true;
        }

        public void LogFrame(Frame frame)
        {
            using (StreamWriter handler = File.AppendText(this.Path))
            {
                handler.WriteLine(frame.Raw.Replace("\r", "").Replace("\n", ""));
            }
        }
    }
}
