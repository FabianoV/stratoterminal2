﻿using StratoTerminal2.Services.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StratoTerminal2.Models.CommandsObjects;
using Newtonsoft.Json;
using System.IO;

namespace StratoTerminal2.Services.Implementation
{
    public class CommandManagementService : ICommandManagementService
    {
        //Properties
        public List<GroupData> Groups { get; set; }
        public string SettingsFilename { get; set; }

        //Constructor
        public CommandManagementService()
        {
            this.Groups = new List<GroupData>();
            this.SettingsFilename = "commands.json";

            if (File.Exists(this.SettingsFilename))
            {
                this.Open();
            }
        }

        public async Task Open()
        {
            string text = File.ReadAllText(this.SettingsFilename);
            this.Groups = JsonConvert.DeserializeObject<List<GroupData>>(text);
        }

        public async Task Save()
        {
            string text = JsonConvert.SerializeObject(this.Groups, Formatting.Indented);
            File.WriteAllText(this.SettingsFilename, text);
        }
    }
}
