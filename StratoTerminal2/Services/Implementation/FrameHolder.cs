﻿using StratoTerminal2.Services.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StratoTerminal2.Models;

namespace StratoTerminal2.Services.Implementation
{
    public class FrameHolder : IFrameHolder
    {
        //Properties
        public List<Frame> Frames { get; set; }
        public object FramesLock { get; set; }
        public Frame LastFrame { get { return this.Frames.LastOrDefault(); } }

        //Constructor
        public FrameHolder()
        {
            this.Frames = new List<Frame>();
        }

        //Methods
        public void AddFrame(Frame frame)
        {
            this.Frames.Add(frame);
        }

        public Frame GetLastFrameByHeight(string frameId, double frameHeight, DateTime frameGPSTime)
        {
            Frame result = null;
            lock (this.Frames)
            {
                result = Frames.LastOrDefault(f => frameId != f.Id && frameHeight != double.NaN && frameGPSTime != DateTime.MinValue);
            }
            return result;
        }
    }
}
