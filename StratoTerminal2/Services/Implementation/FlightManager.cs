﻿using StratoTerminal2.Services.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StratoTerminal2.Models;
using StratoTerminal2.Models.AzureModels;
using StratoTerminal2.Models.RequestsResults;
using StratoTerminal2.Services.Contract.Api;
using Newtonsoft.Json;
using System.IO;

namespace StratoTerminal2.Services.Implementation
{
    public class FlightManager : IFlightManager
    {
        //Services
        public IFlightsApi FlightsApi { get; set; }
        public IInternetConectionService InternetConnectionService { get; set; }

        //Properties
        public List<Flight> Flights { get; set; }
        public Flight SelectedFlight { get; set; }

        //Async Operations Handlers
        public Task SynchronizingFlights { get; set; }

        //Constructor
        public FlightManager(IFlightsApi flightsApi, IInternetConectionService internetConnectionService)
        {
            //Services
            this.FlightsApi = flightsApi;
            this.InternetConnectionService = internetConnectionService;

            //Initialize
            this.Flights = new List<Flight>();
            this.SynchronizingFlights = this.Synchronize();
            this.LoadCachedSelectedFlight();
        }

        //Methods
        public bool SelectFlight(int flightId)
        {
            foreach (Flight flight in Flights)
            {
                if (flightId == flight.Id)
                {
                    this.SelectedFlight = flight;
                    this.SaveToCacheSelectedFlight();
                    return true;
                }
            }

            return false;
        }

        public async Task Synchronize()
        {
            if (InternetConnectionService.CheckInternetConnection() == true)
            {
                GetFlightsRequestResult flightsResult = await this.FlightsApi.GetFlights();
                this.Flights.Clear();
                this.Flights.AddRange(flightsResult.Flights);
            }
        }

        public void SaveToCacheSelectedFlight()
        {
            if (this.SelectedFlight != null)
            {
                string json = JsonConvert.SerializeObject(this.SelectedFlight);
                File.WriteAllText("SelectedGlobalFlightCache.json", json);
            }
        }

        public void LoadCachedSelectedFlight()
        {
            if (File.Exists("SelectedGlobalFlightCache.json"))
            {
                string json = File.ReadAllText("SelectedGlobalFlightCache.json");
                this.SelectedFlight = JsonConvert.DeserializeObject<Flight>(json);
            }
        }
    }
}
