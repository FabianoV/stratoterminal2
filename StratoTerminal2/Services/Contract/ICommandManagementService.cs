﻿using StratoTerminal2.Models.CommandsObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StratoTerminal2.Services.Contract
{
    public interface ICommandManagementService
    {
        List<GroupData> Groups { get; set; }
        string SettingsFilename { get; set; }

        Task Save();
        Task Open();
    }
}
