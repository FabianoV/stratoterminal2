﻿using StratoTerminal2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StratoTerminal2.Services.Contract
{
    public interface IFlightFactory
    {
        Task<Flight> CreateFlight(string name, string deviceName, string STInscanceName, string description="", bool test=false);
        Task<Flight> CreateFlight(string name, DateTime start, string deviceName, string STInscanceName, string description, bool test);
    }
}
