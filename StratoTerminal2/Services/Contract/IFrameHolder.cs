﻿using StratoTerminal2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StratoTerminal2.Services.Contract
{
    public interface IFrameHolder
    {
        List<Frame> Frames { get; }
        Frame LastFrame { get; }

        void AddFrame(Frame frame);

        Frame GetLastFrameByHeight(string frameId, double frameHeight, DateTime frameGPSTime);
    }
}
