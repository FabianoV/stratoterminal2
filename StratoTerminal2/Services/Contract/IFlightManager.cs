﻿using StratoTerminal2.Models;
using StratoTerminal2.Models.AzureModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StratoTerminal2.Services.Contract
{
    public interface IFlightManager
    {
        Flight SelectedFlight { get; set; }
        List<Flight> Flights { get; }

        Task Synchronize();
        bool SelectFlight(int flightId);
    }
}
