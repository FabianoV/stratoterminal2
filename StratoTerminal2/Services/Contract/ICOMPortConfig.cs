﻿using StratoTerminal2.Models;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StratoTerminal2.Services.Contract
{
    public interface ICOMPortConfig
    {
        SerialPort Port { get; }
        bool IsPortOpen { get; }
        Frame LastFrame { get; }

        void CreatePort(string portName, int baudRate);
        void ClosePort();
        List<string> GetComPortsNames();
    }
}
