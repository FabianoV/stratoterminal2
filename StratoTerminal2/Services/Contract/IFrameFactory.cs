﻿using StratoTerminal2.Models;
using StratoTerminal2.Models.EventArgs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StratoTerminal2.Services.Contract
{
    public interface IFrameFactory
    {
        Frame Create(string rawFrame);

        event EventHandler<FrameCreatedEventArgs> FrameCreated;
    }
}
