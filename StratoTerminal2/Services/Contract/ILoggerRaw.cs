﻿using StratoTerminal2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StratoTerminal2.Services.Contract
{
    public interface ILoggerRaw
    {
        string Path { get; }
        bool IsConfigured { get; }

        void LogFrame(Frame frame);
        void Configure();
        void Configure(string path);
    }
}
