﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StratoTerminal2.Services.Contract
{
    public interface IMessageBoxService
    {
        void Show(string message);
    }
}
