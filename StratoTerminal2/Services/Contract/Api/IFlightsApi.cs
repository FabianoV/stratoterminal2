﻿using StratoTerminal2.Models;
using StratoTerminal2.Models.RequestsResults;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StratoTerminal2.Services.Contract.Api
{
    public interface IFlightsApi : IApi
    {
        Task<GetFlightsRequestResult> GetFlights();
        Task<GetFlighRequestResult> GetFlight(int flightId);
        Task<CreateFlightRequestResult> CreateFlight(Flight flight);
        Task<UpdateFlightRequestResult> UpdateFlight(Flight flight);
        Task<DeleteFlightRequestResult> DeleteFlight(int flightId);
    }
}
