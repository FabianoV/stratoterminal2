﻿using StratoTerminal2.Models;
using StratoTerminal2.Models.RequestsResults;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StratoTerminal2.Services.Contract.Api
{
    public interface IFramesApi : IApi
    {
        Task<GetFramesFromFlightRequestResult> GetFrames(int flightId);
        Task<GetFrameRequestResult> GetFrame(int frameDbId);
        Task<GetFrameRequestResult> GetFrame(string frameId, int flightId);
        Task<CreateFrameRequestResult> CreateFrame(Frame frame);
        Task<UpdateFrameRequestResult> UpdateFrame(Frame frame);
        Task<DeleteFrameRequestResult> DeleteFrame(int frameId);
        Task<DeleteFrameRequestResult> DeleteFrame(string frameId, int flightId);
    }
}
