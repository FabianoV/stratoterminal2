﻿using StratoTerminal2.Models;
using StratoTerminal2.Models.RequestsResults;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StratoTerminal2.Services.Contract.Api
{
    public interface IBingMapApi : IApi
    {
        Task<GetElevationRequestResult> GetElevation(Location location);
    }
}
