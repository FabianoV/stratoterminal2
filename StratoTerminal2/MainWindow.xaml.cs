﻿using Awesomium.Core;
using GalaSoft.MvvmLight.Ioc;
using Microsoft.Maps.MapControl.WPF;
using StratoTerminal2.Models.RequestsResults;
using StratoTerminal2.Services.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace StratoTerminal2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        //private void Map_MouseMove(object sender, MouseEventArgs e)
        //{
        //    //Get the mouse click coordinates
        //    Point mousePosition = e.GetPosition(this);

        //    //Convert the mouse coordinates to a locatoin on the map
        //    Location location = Map.ViewportPointToLocation(mousePosition);

        //    this.LattitudeInfo.Text = location.Latitude.ToString();
        //    this.LongtitudeInfo.Text = location.Longitude.ToString();
        //}

        //private async void Map_MouseDown(object sender, MouseButtonEventArgs e)
        //{
        //    //Get the mouse click coordinates
        //    Point mousePosition = e.GetPosition(this);

        //    //Convert the mouse coordinates to a locatoin on the map
        //    Location location = Map.ViewportPointToLocation(mousePosition);
        //    Models.Location STLocation = new Models.Location()
        //    {
        //        Lattitude = location.Latitude,
        //        Longtitude = location.Longitude,
        //    };

        //    IApiService apiService = SimpleIoc.Default.GetInstance<IApiService>();
        //    GetElevationRequestResult result = await apiService.GetElevation(STLocation);

        //    if (result.HeightFound)
        //    {
        //        this.HeightInfo.Text = result.Location.Height.ToString();
        //    }
        //}

        private void ConsoleInfoBoxScrollViewer_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            // User scroll event : set or unset autoscroll mode
            if (e.ExtentHeightChange == 0)
            {   // Content unchanged : user scroll event
                if (ConsoleInfoBoxScrollViewer.VerticalOffset == ConsoleInfoBoxScrollViewer.ScrollableHeight)
                {   // Scroll bar is in bottom - Set autoscroll mode
                    this.AutoScroll.IsChecked = true;
                }
                else
                {   // Scroll bar isn't in bottom - Unset autoscroll mode
                    this.AutoScroll.IsChecked = false;
                }
            }

            // Content scroll event : autoscroll eventually
            if (this.AutoScroll.IsChecked.Value && e.ExtentHeightChange != 0)
            {   // Content changed and autoscroll mode set Autoscroll
                ConsoleInfoBoxScrollViewer.ScrollToVerticalOffset(ConsoleInfoBoxScrollViewer.ExtentHeight);
            }
        }

        private void AutoScroll_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void ComPorts_DropDownOpened(object sender, EventArgs e)
        {
            ((sender as ComboBox).DataContext as ViewModel.SettingsPanel).UpdateComPorts();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Do you want to close this window?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result != MessageBoxResult.Yes)
            {
                e.Cancel = true;
            }
        }

        private void FlightSettingsTabItem_Loaded(object sender, RoutedEventArgs e)
        {
            ViewModel.FlightSettings vm = SimpleIoc.Default.GetInstance<ViewModel.FlightSettings>();
            vm.FlightSettingsLoadedCommand.Execute(null);
        }

        private void FramesSettingsTabItem_Loaded(object sender, RoutedEventArgs e)
        {
            ViewModel.FramesSettings vm = SimpleIoc.Default.GetInstance<ViewModel.FramesSettings>();
            vm.FramesSettingsLoadedCommand.Execute(null);
        }

        //private void MapAddHeight_Click(object sender, RoutedEventArgs e)
        //{
        //    this.Map.ZoomLevel++;
        //}

        //private void MapSubHeight_Click(object sender, RoutedEventArgs e)
        //{
        //    this.Map.ZoomLevel--;
        //}


    }
}
