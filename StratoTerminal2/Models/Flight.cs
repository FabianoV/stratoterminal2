﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StratoTerminal2.Models
{
    public class Flight
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime StartTime { get; set; }
        public string DeviceName { get; set; }
        public string Description { get; set; }
        public int IsTest { get; set; }
        public string STInstanceName { get; set; }
    }
}
