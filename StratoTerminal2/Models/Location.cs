﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StratoTerminal2.Models
{
    public class Location
    {
        public double Lattitude { get; set; }
        public double Longtitude { get; set; }
        public double Height { get; set; }
    }
}
