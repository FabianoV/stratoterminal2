﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StratoTerminal2.Models.Enums
{
    public enum FrameState
    {
        Data = 0,
        RadioOff = 1,
        RadioOn = 2,
        InitOk = 3,
        CodeVersion = 4,
        Error = 5,
        Damaged = 6,
        DataOnDemand = 7,
    }
}
