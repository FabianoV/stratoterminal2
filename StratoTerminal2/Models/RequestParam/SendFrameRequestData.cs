﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StratoTerminal2.Models.RequestParam
{
    public class SendFrameRequestData
    {
        public int Id { get; set; }
        public int IdLotu { get; set; }
        public int NrRamki { get; set; }
        public DateTime Czas { get; set; }
        public double TemperaturaWewnetrzna { get; set; }
        public double TemperaturaZewnetrzna { get; set; }
        public double Bateria { get; set; }
        public int Fix { get; set; }
        public int Satelity { get; set; }
        public int Wysokosc { get; set; }
        public double PozycjaDlugosc { get; set; }
        public double PozycjaSzerokosc { get; set; }
        public double PredkoscWertykalna { get; set; }
        public double PredkoscHoryzontalna { get; set; }
        public double Wznoszenie { get; set; }
        public string IOContol { get; set; }
        public string DanePomiarowe { get; set; }
        public string SurowaRamka { get; set; }
        public string StatusRamki { get; set; }
    }
}
