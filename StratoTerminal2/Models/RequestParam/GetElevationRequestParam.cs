﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StratoTerminal2.Models.RequestParam
{
    public class GetElevationRequestParam
    {
        public double lat1 { get; set; }
        public double long1 { get; set; }
        public double lat2 { get; set; }
        public double long2 { get; set; }
        public int MyProperty { get; set; }
    }
}
