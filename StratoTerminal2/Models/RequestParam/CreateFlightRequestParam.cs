﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StratoTerminal2.Models.RequestParam
{
    public class CreateFlightRequestParam
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Start { get; set; }
        public string DeviceName { get; set; }
        public string Description { get; set; }
        public bool IsTest { get; set; }
        public string STInstanceName { get; set; }
    }
}
