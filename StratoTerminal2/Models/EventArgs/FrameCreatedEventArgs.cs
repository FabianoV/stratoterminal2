﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StratoTerminal2.Models.EventArgs
{
    public class FrameCreatedEventArgs
    {
        public Frame LastFrame { get; set; }
        public Frame NewFrame { get; set; }
    }
}
