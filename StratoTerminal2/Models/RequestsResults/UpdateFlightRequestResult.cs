﻿using StratoTerminal2.Models.AzureModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace StratoTerminal2.Models.RequestsResults
{
    public class UpdateFlightRequestResult : BaseRequestResult
    {
        // set true when server says in json tha frame was succesfully updated
        public bool IsSuccessfulyUpdated { get; set; }
        public Flight UpdatedFlight { get; set; }

        public override void MapResult(string responseMessage, HttpStatusCode code, string usedToken, string error)
        {
            base.MapResult(responseMessage, code, usedToken, error);
        }
    }
}
