﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using StratoTerminal2.Models.Helper;
using Newtonsoft.Json;

namespace StratoTerminal2.Models.RequestsResults
{
    public class GetElevationRequestResult : BaseRequestResult
    {
        public Location Location { get; set; }
        public bool HeightFound { get; set; }

        public override void MapResult(string responseMessage, HttpStatusCode code, string usedToken, string error)
        {
            base.MapResult(responseMessage, code, usedToken, error);

            if (code == HttpStatusCode.OK)
            {
                BingElevationJsonObject data = JsonConvert.DeserializeObject<BingElevationJsonObject>(responseMessage);

                if (data.statusDescription != "OK")
                {
                    this.HandleError(new Exception("Something was wrong. Maybe bing api key is invalid???"));
                }

                if (data.resourceSets.Count > 0 && data.resourceSets[0].resources.Count > 0)
                {
                    this.HeightFound = true;
                    this.Location = new Location()
                    {
                        Height = data.resourceSets[0].resources[0].elevations[0],
                    };
                }
                else
                {
                    this.HeightFound = false;
                    this.Location = new Location() { Height = -1, };
                }
            }
            else
            {
                this.SetNotImplementedError();
            }
        }
    }
}
