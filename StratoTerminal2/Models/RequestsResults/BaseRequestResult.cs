﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace StratoTerminal2.Models.RequestsResults
{
    public abstract class BaseRequestResult
    {
        //public string UsedToken { get; set; }
        public HttpStatusCode StatusCode { get; set; }
        public string ResponseMessage { get; set; }

        public Exception Exception { get; set; }
        public bool HasError { get; set; }
        public string ErrorMessage { get; set; }
        public int ErrorNr { get; set; }

        public void HandleError(Exception ex)
        {
            this.HasError = true;
            this.Exception = ex;
        }

        public virtual void MapResult(string responseMessage, HttpStatusCode code, string usedToken, string error)
        {
            this.StatusCode = code;
            //this.UsedToken = usedToken;
            this.ErrorMessage = error;
            this.ResponseMessage = responseMessage;

            if (!string.IsNullOrWhiteSpace(error))
            {
                this.HasError = true;
            }
        }

        protected virtual void SetAuthenticationError()
        {
            this.HasError = true;
            this.ErrorMessage = "Błąd autoryzacji po stronie serwera.";
            this.ErrorNr = 0;
        }

        protected virtual void SetNotImplementedError()
        {
            this.HasError = true;
            this.ErrorMessage = "Przypadek nie został zaimplementowany. Status Code: " + this.StatusCode;
            this.ErrorNr = 0;
        }

        public virtual void SetNoInternetConnectionError()
        {
            this.HasError = true;
            this.ErrorMessage = "Brak połaczenia z internetem. Zapytanie nie zostao zrealizowane.";
            this.ErrorNr = 0;
        }

        public virtual void AfterMapResult()
        {
            if (this.HasError && this.ErrorMessage != null && this.ErrorNr != 0)
            {
                Debug.WriteLine("(" + this.ErrorNr + ") API Error: " + this.ErrorMessage);
            }
            else
            {
                if (this.HasError)
                {
                    Debug.WriteLine("Nieznany błąd API.");
                }
            }
        }


    }
}
