﻿using Newtonsoft.Json;
using StratoTerminal2.Models.AzureModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace StratoTerminal2.Models.RequestsResults
{
    public class GetFlighRequestResult : BaseRequestResult
    {
        public Flight Flight { get; set; }

        public override void MapResult(string responseMessage, HttpStatusCode code, string usedToken, string error)
        {
            base.MapResult(responseMessage, code, usedToken, error);

            if (code == HttpStatusCode.OK)
            {
                this.Flight = JsonConvert.DeserializeObject<Flight>(responseMessage);
            }
            else if (code == HttpStatusCode.Unauthorized)
            {
                this.SetAuthenticationError();
            }
            else
            {
                throw new NotImplementedException();
            }
        }
    }
}
