﻿using Newtonsoft.Json;
using StratoTerminal2.Models.AzureModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace StratoTerminal2.Models.RequestsResults
{
    public class GetFlightsRequestResult : BaseRequestResult
    {
        public List<Flight> Flights { get; set; }

        public override void MapResult(string responseMessage, HttpStatusCode code, string usedToken, string error)
        {
            base.MapResult(responseMessage, code, usedToken, error);

            if (!string.IsNullOrWhiteSpace(responseMessage))
            {
                if (code == HttpStatusCode.OK)
                {
                    this.Flights = JsonConvert.DeserializeObject<List<Flight>>(responseMessage);
                }
                else if (code == HttpStatusCode.Unauthorized)
                {
                    this.SetAuthenticationError();
                }
                else
                {
                    throw new NotImplementedException();
                }
            }
        }
    }
}
