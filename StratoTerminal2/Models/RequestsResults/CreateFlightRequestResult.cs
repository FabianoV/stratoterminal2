﻿using Newtonsoft.Json;
using StratoTerminal2.Models.AzureModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace StratoTerminal2.Models.RequestsResults
{
    public class CreateFlightRequestResult : BaseRequestResult
    {
        // set true when server write in jeson that flight is created
        public bool IsFlightCreatedSuccessfully { get; set; }

        //created flight in json server response
        public Flight Flight { get; set; }

        public override void MapResult(string responseMessage, HttpStatusCode code, string usedToken, string error)
        {
            base.MapResult(responseMessage, code, usedToken, error);

            if (code == HttpStatusCode.OK)
            {
                this.Flight = JsonConvert.DeserializeObject<Flight>(responseMessage);
                this.IsFlightCreatedSuccessfully = true;
            }
            else if (code == HttpStatusCode.Unauthorized)
            {
                this.SetAuthenticationError();
            }
            else
            {
                this.SetNotImplementedError();
            }
        }
    }
}
