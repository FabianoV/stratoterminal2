﻿using StratoTerminal2.Models.AzureModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace StratoTerminal2.Models.RequestsResults
{
    public class CreateFrameRequestResult : BaseRequestResult
    {
        //Is true when server response that frame is succesfully saven in database.
        public bool IsSuccessfulyCreated { get; set; }
        public int FrameDbId { get; set; }
        public FrameAzure Frame { get; set; }

        public override void MapResult(string responseMessage, HttpStatusCode code, string usedToken, string error)
        {
            base.MapResult(responseMessage, code, usedToken, error);


        }
    }
}
