﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StratoTerminal2.Models.CommandsObjects
{
    public class GroupData
    {
        public string GroupName { get; set; }
        public List<CommandData> Commands { get; set; }
    }
}
