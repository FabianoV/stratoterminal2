﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StratoTerminal2.Models.CommandsObjects
{
    public class CommandData
    {
        public string Name { get; set; }
        public string Content { get; set; }
        public string Description { get; set; }
    }
}
