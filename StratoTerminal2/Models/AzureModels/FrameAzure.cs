﻿using StratoTerminal2.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StratoTerminal2.Models.AzureModels
{
    public class FrameAzure
    {
        public int FrameId { get; set; }

        public string[] Data { get; set; }
        public string Raw { get; set; }
        public DateTime StratoTerminalTime { get; set; }
        public FrameState State { get; set; }

        public DateTime GPSTime { get; set; }
        public string Id { get; set; }
        public double InnerTemperature { get; set; }
        public double OuterTemperature { get; set; }
        public double Battery { get; set; }
        public int Fix { get; set; }
        public int SatelitesCount { get; set; }
        public double Height { get; set; }
        public double Lattitude { get; set; }
        public double Longtitude { get; set; }
        public double VerticalSpeed { get; set; }
        public double HorizontalSpeed { get; set; }
        public double Ascent { get; set; }
        public string IOControl { get; set; }
        public string DataPayload { get; set; }

        public int FlightId { get; set; }
    }
}
