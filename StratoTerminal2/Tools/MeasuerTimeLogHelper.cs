﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace StratoTerminal2.Tools
{
    public class MeasuerTimeLogHelper
    {
        public DateTime RequestStartTime { get; set; }
        public string RequestUri { get; set; }
        public int RequestId { get; set; }
        public TimeSpan RequestElapsedTime { get; set; }

        public MeasuerTimeLogHelper(string requestUri, string requestBody = "")
        {
            this.RequestStartTime = DateTime.Now;
            this.RequestId = GetRequestId();

            Debug.WriteLine(this.RequestId + " Params: " + requestBody);
            Debug.WriteLine(this.RequestId + " Adress: " + requestUri + " at " + this.RequestStartTime);
        }

        private static int GetRequestId()
        {
            Random rnd = new Random();
            return rnd.Next(1000, 10000);
        }

        public void FlagRequestDownloaded(string response)
        {
            Debug.WriteLine(this.RequestId + " Response: " + response);
        }

        public void EndMeasure()
        {
            DateTime requestEnd = DateTime.Now;
            this.RequestElapsedTime = requestEnd - this.RequestStartTime;
            Debug.WriteLine("Create, rId: " + RequestId + ": End request: " + this.RequestUri + " at " + this.RequestStartTime + ". Time: " + this.RequestElapsedTime);
        }
    }
}
