﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace StratoTerminal2.Tools
{
    public static class Extension
    {
        //Short Conversion
        public static int ToInt(this string text)
        {
            if (string.IsNullOrWhiteSpace(text))
            {
                return int.MinValue;
            }

            if (text.Contains("?"))
            {
                return int.MinValue;
            }

            return Convert.ToInt32(text);
        }
        public static double ToDouble(this string text)
        {
            text = text.Replace(".", ",");

            if (string.IsNullOrWhiteSpace(text))
            {
                return double.NaN;
            }

            if (text.Contains("?"))
            {
                return double.NaN;
            }

            return Convert.ToDouble(text);
        }
        public static DateTime ToDateTimeUTC(this string text)
        {
            if (string.IsNullOrWhiteSpace(text))
            {
                return DateTime.MinValue;
            }

            try
            {
                DateTime timeWithNoCulture = DateTime.ParseExact(text, "HHmmss", CultureInfo.InvariantCulture);
                return DateTime.SpecifyKind(timeWithNoCulture, DateTimeKind.Utc);
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public static string CommaToDot(this double value)
        {
            return value.ToString().Replace(",", ".");
        }
        public static string CommaToDot(this string value)
        {
            return value.Replace(",", ".");
        }

        //Fluent interface for ObservableCollection
        public static ObservableCollection<TSource> ToObservable<TSource>(this List<TSource> list)
        {
            if (list == null)
            {
                return null;
            }
            ObservableCollection<TSource> obs = new ObservableCollection<TSource>();
            foreach (var item in list)
            {
                obs.Add(item);
            }
            return obs;
        }
        public static ObservableCollection<TSource> ClearF<TSource>(this ObservableCollection<TSource> collection)
        {
            collection.Clear();
            return collection;
        }
        public static ObservableCollection<T> AddRange<T>(this ObservableCollection<T> collection, List<T> items) where T : class
        {
            foreach (T item in items)
            {
                collection.Add(item);
            }

            return collection;
        }
        public static ObservableCollection<T> AddRangeAtTop<T>(this ObservableCollection<T> collection, List<T> items) where T : class
        {
            List<T> temp = collection.ToList();
            collection.Clear();
            collection.AddRange(items);
            collection.AddRange(temp);
            return collection;
        }

        //Shallow object copy extension
        public static T Clone<T>(this T obj) where T : class
        {
            if (obj == null)
            {
                return null;
            }

            MethodInfo inst = obj.GetType().GetMethod("MemberwiseClone", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
            return (T)inst?.Invoke(obj, null);
        }
    }
}
