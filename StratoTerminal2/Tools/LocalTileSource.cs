﻿using Microsoft.Maps.MapControl.WPF;
using Microsoft.Maps.MapControl.WPF.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace StratoTerminal2.Tools
{
    //public class LocalTileSource : TileSource
    //{
    //    //Fields
    //    private string tilePath = string.Empty;

    //    //Constructor
    //    public LocalTileSource(string tilePath)
    //    {
    //        this.tilePath = tilePath;

    //        this.DirectImage = new ImageCallback(TileRender);
    //    }

    //    //Methods
    //    public BitmapImage TileRender(long x, long y, int zoomLevel)
    //    {
    //        if (!string.IsNullOrWhiteSpace(this.tilePath))
    //        {
    //            var q = new QuadKey((int)x, (int)y, zoomLevel);

    //            var filePath = this.tilePath.Replace("{quadkey}", q.Key).Replace("{x}", x.ToString()).Replace("{y}", y.ToString()).Replace("{zoomlevel}", zoomLevel.ToString());

    //            FileInfo file = new FileInfo(filePath);
    //            if (file.Exists)
    //            {
    //                using (var fileStream = file.OpenRead())
    //                {
    //                    BitmapImage bmp = new BitmapImage();
    //                    bmp.BeginInit();
    //                    bmp.StreamSource = fileStream;
    //                    bmp.CacheOption = BitmapCacheOption.OnLoad;
    //                    bmp.EndInit();
    //                    return bmp;
    //                }
    //            }
    //        }

    //        return CreateTransparentTile();
    //    }

    //    private BitmapImage CreateTransparentTile()
    //    {
    //        int width = 256;
    //        int height = 256;

    //        var source = BitmapSource.Create(width, height, 96, 96, PixelFormats.Indexed1,
    //                                        new BitmapPalette(new List<Color>(){ Colors.Transparent }),
    //                                        new byte[width * height], width);

    //        var image = new BitmapImage();
    //        var memoryStream = new MemoryStream();

    //        PngBitmapEncoder encoder = new PngBitmapEncoder();
    //        encoder.Frames.Add(BitmapFrame.Create(source));
    //        encoder.Save(memoryStream);

    //        memoryStream.Position = 0;

    //        image.BeginInit();
    //        image.StreamSource = memoryStream;
    //        image.EndInit();

    //        return image;
    //    }
    //}
}
