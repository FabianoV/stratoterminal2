﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace StratoTerminal2.Converters
{
    public class DoubleBatteryToInformation : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            

            double valueDouble = System.Convert.ToDouble(value);
            if (double.IsNaN(valueDouble))
            {
                return valueDouble;
            }
            return value + " V";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
